var gradeChapter = "19NA04_c10";
var chapterSection = "s03";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [	
	{
		'id':'screen1', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01', 
			'captionText':"Plot each fraction or decimal on a number line. Three over ten.Thirty over one hundred.Three tenths.Thirty hundredths."
		}]
	},
	{	
		'id':'screen2', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02', 
			'captionText':"What do you notice about the locations of the points? What can you conclude about the numbers?."
		}]
	}
];