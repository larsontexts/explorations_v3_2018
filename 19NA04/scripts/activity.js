/* global $ */
/* global ShellModel */
"use strict";

function OnResize() {
  var winHeight = $(window).height();
  $('.will-scroll').height(winHeight - 225);
}
$(window).on('resize', OnResize);

/**
 * Activity controller.
 * @constructor
 */
function ActivityModel() {
  var ref = this;
  ref.init(7);
  $('.activity-start-screen .start-btn').on('click', function() {
    $('.activity-start-screen').hide();
    $('.activity').show();
    ref.startActivity();
  });

  OnResize();
}
ActivityModel.prototype = new ShellModel();
ActivityModel.prototype.constructor = ActivityModel;

/**
 * Standard learnosity question.
 */
ActivityModel.prototype.setupLearnosityCheck = function($parent, id, Donebtn) {
  var ref = this;
  var $checkBtn = this.checkBtn = $parent.find('.btn-check');
  var $tryBtn = this.tryBtn = $parent.find('.btn-try-again');
  var $showBtn = this.showBtn = $parent.find('.btn-show-me');
  var $question = $parent.find('.question');
  var $answer = $parent.find('.answer');
  var $overlay = $parent.find('.understanding-overlay');

  // update the button visibility
  ref.showCheckAnswerButtons(true);
  if (Donebtn===true) {
    ref.disableDoneButton(true);
  } else {
    ref.disableNextButton(true);
  }

  $question.show();
  $answer.hide();
  ref.enableQuestion(id);
  ref.removeValidationStyles();

  $checkBtn.off('click');
  $checkBtn.on('click', function() {
    var isCorrect = ref.checkAnswer(id);
    if (isCorrect) {
      $question.hide();
      $answer.show();

      ref.removeValidationStyles();
      $overlay.addClass('correct');
      ref.showCheckAnswerCorrectButtons();
      if (Donebtn===true) {
        ref.disableDoneButton(false);
      } else {
        ref.disableNextButton(false);
      }
    } else {
      ref.showCheckAnswerIncorrectButtons();
    }
  });

  $tryBtn.off('click');
  $tryBtn.on('click', function() {
    ref.enableQuestion(id);
    ref.removeValidationStyles();
    ref.showTryAgainButtons(true);
  });

  $showBtn.off('click');
  $showBtn.on('click', function() {
    $question.hide();
    $answer.show();

    ref.removeValidationStyles();

    if (Donebtn===true) {
      ref.disableDoneButton(false);
    } else {
      ref.disableNextButton(false);
    }
    ref.showShowAnswerButtons();
    $overlay.addClass('correct');
  });
};

/**
 * Step 1: Intro Screen
 */
ActivityModel.prototype.step1Handler = function() {
  this.disableNextButton(false);
  $('.right-side').hide();
};

/**
 * Step 2: Drag counters to model 24 * 3.
 */
ActivityModel.prototype.step2Handler = function() {
  var $elem = $(".activity-slide[data-slide='2']");
  $('.will-scroll').removeClass('scroll-height');

  // Init variables
  var $checkBtn = this.checkBtn = $elem.find('.btn-check');
  var $tryBtn = this.tryBtn = $elem.find('.btn-try-again');
  var $showBtn = this.showBtn = $elem.find('.btn-show-me');
  var $dropArea = $elem.find('.drop-area1 .drop-area-block1');
  var $dragBin = $elem.find('.drag-bin1');

  $dragBin.show();

  this.removeValidationStyles();
  this.showCheckAnswerButtons(true); // Set button default states
  this.updateCheck1();
  this.disableNextButton(true);
  this.createDragDrop($dropArea, $dragBin, this.updateCheck1.bind(this));
  this.removeValidationStyles();

  // Add click handlers.
  $checkBtn.off('click').on('click', this.step2CheckAnswer.bind(this));
  $tryBtn.off('click').on('click', this.step2TryAgain.bind(this));
  $showBtn.off('click').on('click', this.step2ShowAnswer.bind(this));
};

/**
 * Check the answer
 * @return {boolean}
 * @private
 */

ActivityModel.prototype.step2IsCorrect = function() {
  var $elem = $(".activity-slide[data-slide='2']");
  var $dropArea = $elem.find('.drop-area1 .drop-area-block1');

  var tens = $dropArea.find('.tens-unit').length;
  var ones = $dropArea.find('.ones-unit').length;
  return (tens*10 + ones === 72);
};

/**
 * Step 2: Check Answer handler.
 */
ActivityModel.prototype.step2CheckAnswer = function() {
  var $elem = $(".activity-slide[data-slide='2']");
  var $dropArea = $elem.find('.drop-area1 .drop-area-block1');
  var $dragBin = $elem.find('.drag-bin1');

  // disable dragging
  $dragBin.hide();
  $dropArea.find('.draggable-item').draggable('disable');

  // Check answer.
  if (this.step2IsCorrect()) {
    $dropArea.addClass('correct');
    this.showCheckAnswerCorrectButtons();
    this.disableNextButton(false);

  } else {
    $dropArea.addClass('incorrect');
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 * Step 2: Try Again handler.
 */
ActivityModel.prototype.step2TryAgain = function() {
  this.removeValidationStyles();
  this.showTryAgainButtons(true); // Update button visibility.

  var $elem = $(".activity-slide[data-slide='2']");
  var $dropArea = $elem.find('.drop-area1 .drop-area-block1');
  var $dragBin = $elem.find('.drag-bin1');

  $dragBin.show(); // Re-enable the dragging.
  $dropArea.find('.draggable-item').draggable('enable'); // Re-enable the draggable items.
};

/**
 * Step 2: Show Answer handler.
 */
ActivityModel.prototype.step2ShowAnswer = function() {
  this.removeValidationStyles();
  this.disableNextButton(false);
  this.showShowAnswerButtons();

  var $elem = $(".activity-slide[data-slide='2']");
  var $dropArea = $elem.find('.drop-area1 .drop-area-block1');
  var $dragBin = $elem.find('.drag-bin1');

  // Update drop area
  $dropArea.empty(); // Remove all items.

  // Add the proper amount to the drop area.
  this.showSol1($dragBin.find('.draggable-item'), $dropArea);
  $dropArea.removeClass('inside');
  $dropArea.addClass('correct');
};

/**
 * Updates the Slide 2 check button enabled/disabled state.
 */
ActivityModel.prototype.updateCheck1 = function() {

  var $elem = $(".activity-slide[data-slide='2']");
  var $dropArea = $elem.find('.drop-area1 .drop-area-block1');
  this.showCheckAnswerButtons($dropArea[0].childElementCount > 0);
};

/**
 * Show the solution for 72.
 */
ActivityModel.prototype.showSol1 = function($dragItems, $dropArea) {

  var i;
  var $item;
  var $clone;
  var unitWidth = 40;
  var unitHeight = 40;

  var alignLeft = 0;
  var alignTop = 0;
  $item = $dragItems.eq(0).clone();
  $item.show();
  for (i=0; i<7; i+=1) {
    $clone = $item.clone();
    $clone.appendTo($dropArea);
    $clone.css('left', alignLeft + i*unitWidth);
  }

  $item = $dragItems.eq(1).clone();
  $item.show();
  for (i=0; i<2; i+=1) {
    $clone = $item.clone();
    $clone.appendTo($dropArea);
    var left = alignLeft + 7*unitWidth + Math.floor(i/5)*unitWidth;
    var top = alignTop + (i%5)*unitHeight;
    $clone.css('left', left);
    $clone.css('top', top);
  }
};

/**
 * Show the solution for 120.
 */
ActivityModel.prototype.showSol2 = function($dragItems, $dropArea) {

  var i;
  var $item;
  var $clone;
  var unitWidth = 137;
  var unitWidth2 = 50;

  var alignLeft = 0;
  $item = $dragItems.eq(0).clone();
  $item.show();
  for (i=0; i<1; i+=1) {
    $clone = $item.clone();
    $clone.appendTo($dropArea);
    $clone.css('left', alignLeft + i*unitWidth);
  }

  $item = $dragItems.eq(1).clone();
  $item.show();
  for (i=0; i<2; i+=1) {
    $clone = $item.clone();
    $clone.appendTo($dropArea);
    var left = unitWidth + alignLeft + i*unitWidth2;
    $clone.css('left', left);
  }
};

/**
 * Step 3: Question.
 */
ActivityModel.prototype.step3Handler = function() {
  var $elem = $(".activity-slide[data-slide='3']");
  this.setupLearnosityCheck($elem, '19NA04_c03s07_exp_int_exploration_01', false);
  $('.right-side').show();
};

/**
 * Step 4: Essay answer.
 */
ActivityModel.prototype.step4Handler = function() {
  $('.will-scroll').addClass('scroll-height');

  this.scrollToBottom($('.will-scroll'));
  this.removeValidationStyles();
  this.disableNextButton(true);
  this.enableQuestion('19NA04_c03s07_exp_int_exploration_02');

  // Add save handler.
  var $elem = $(".activity-slide[data-slide='4'] ");
  var $saveBtn = $elem.find('.btn-save');
  this.initSaveButton($saveBtn, this.step4SaveHandler.bind(this));
};

/**
 * Step 4: Save handler.
 */
ActivityModel.prototype.step4SaveHandler = function() {
  this.disableNextButton(false); // Enable the Next button.
  this.disableQuestion('19NA04_c03s07_exp_int_exploration_02'); // Disable the essay input.
};

/**
 * Step 5: 15 * 8.
 */
ActivityModel.prototype.step5Handler = function() {
  var $elem = $(".activity-slide[data-slide='5']");
  $('.will-scroll').removeClass('scroll-height');

  // Remove all content from previous screen
  $(".activity-slide[data-slide='3']").addClass('hideElement'); // remove the previous equation
  $('.drop-area1 .drop-area-block1').addClass('hideElement'); // hide previous dropArea and its content

  // Update the main question to match the new content
  $('.main_question').addClass('hideElement');

  // Init variables
  var $checkBtn = this.checkBtn = $elem.find('.btn-check');
  var $tryBtn = this.tryBtn = $elem.find('.btn-try-again');
  var $showBtn = this.showBtn = $elem.find('.btn-show-me');
  var $dropArea = $('.drop-area2 .drop-area-block2');
  var $dragBin = $elem.find('.drag-bin2');

  $dragBin.show();

  this.removeValidationStyles();
  this.showCheckAnswerButtons(true); // Set button default states
  this.updateCheck2();
  this.disableNextButton(true);

  //
  this.createDragDrop($dropArea, $dragBin, this.updateCheck2.bind(this));

  this.removeValidationStyles();

  // Add click handler.
  $checkBtn.off('click').on('click', this.step5CheckAnswer.bind(this));
  $tryBtn.off('click').on('click', this.step5TryAgain.bind(this));
  $showBtn.off('click').on('click', this.step5ShowAnswer.bind(this));
};

/**
 * Check the answer
 * @return {boolean}
 * @private
 */
ActivityModel.prototype.step5IsCorrect = function() {
  var $elem = $(".activity-slide[data-slide='5']");
  var $dropArea = $('.drop-area2 .drop-area-block2');

  var hundreds = $dropArea.find('.hundreds-unit').length;
  var tens = $dropArea.find('.tens-unit').length;
  var ones = $dropArea.find('.ones-unit').length;
  return (hundreds*100 + tens*10 + ones === 120);
};

/**
 * Step 5: Check Answer handler.
 */
ActivityModel.prototype.step5CheckAnswer = function() {
  var $elem = $(".activity-slide[data-slide='5']");
  var $dropArea = $('.drop-area2 .drop-area-block2');
  var $dragBin = $elem.find('.drag-bin2');

  // Disable dragging.
  $dragBin.hide();
  $dropArea.find('.draggable-item').draggable('disable');

  // Check answer.
  if (this.step5IsCorrect()) {
    $dropArea.addClass('correct');
    this.showCheckAnswerCorrectButtons();
    this.disableNextButton(false);

  } else {
    $dropArea.addClass('incorrect');
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 * Step 5: Try Again handler.
 */
ActivityModel.prototype.step5TryAgain = function() {
  var $elem = $(".activity-slide[data-slide='5']");
  var $dropArea = $('.drop-area2 .drop-area-block2');
  var $dragBin = $elem.find('.drag-bin2');

  // Re-enable the dragging
  $dragBin.show();

  // Update button visibility.
  this.removeValidationStyles();
  this.showTryAgainButtons(true);

  // Re-enable the draggable items.
  $dropArea.find('.draggable-item').draggable('enable');
};

/**
 * Step 5: Show Answer handler.
 */
ActivityModel.prototype.step5ShowAnswer = function() {
  this.removeValidationStyles();
  this.disableNextButton(false);
  this.showShowAnswerButtons();

  var $elem = $(".activity-slide[data-slide='5']");
  var $dropArea = $('.drop-area2 .drop-area-block2');
  var $dragBin = $elem.find('.drag-bin2');

  // Update drop area
  $dropArea.empty(); // remove all items.

  // Add the proper amount to the drop area.
  this.showSol2($dragBin.find('.draggable-item'), $dropArea);

  $dropArea.removeClass('inside');
  $dropArea.addClass('correct');
};

/**
 * Step 6: 15 * 8.
 */
ActivityModel.prototype.step6Handler = function() {
  var $elem = $(".activity-slide[data-slide='6']");
  this.setupLearnosityCheck($elem, '19NA04_c03s07_exp_int_exploration_03', true);
};

/**
 * Step 7: Listen to audio.
 */
ActivityModel.prototype.step7Handler = function() {
  // Nothing.
  this.removeValidationStyles();
};

/**
 * Updates the Slide 4 check button enabled/disabled state.
 */
ActivityModel.prototype.updateCheck2 = function() {
  var $elem = $(".activity-slide[data-slide='5']");
  var $dropArea = $elem.find('.drop-area2 .drop-area-block2');
  this.showCheckAnswerButtons($dropArea[0].childElementCount > 0);
};

ActivityModel.prototype.clearDraggables = function() {
  $('.activity .drop-area .drop-area-block .draggable-item').remove();
};

/**
 * Reset
 */
ActivityModel.prototype.resetActivityHandler = function() {
  // Remove all blocks from the drop area
  this.clearDraggables();

  this.disableNextButton(false);
  this.disableDoneButton(false);

  // show content of slide3
  $(".activity-slide[data-slide='3']").removeClass('hideElement');

   // show drop area1
  $('.drop-area1 .drop-area-block1').removeClass('hideElement');

  // Reset main question
  $('.main_question').removeClass('hideElement');

  // Clean-up events.
  var audioPlayer = $(".activity-audio[data-slide='6']")[0];
  audioPlayer.removeEventListener('ended', this.waitForAudio);
};

/**
 * Wait until the audio has finished then enable the audio.
 */
ActivityModel.prototype.waitForAudio = function() {
  this.disableDoneButton(false);
};
