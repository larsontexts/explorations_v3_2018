var gradeChapter = "19NA04_c01";
var chapterSection = "s01";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{
		'id':'screen1',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01',
			'captionText':"Model one thousand two hundred seventy-five and three thousand three hundred thirty-three. Then use the models to write the value of each digit"
		}]
	},
	{
		'id':'screen2',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02',
			'captionText':"Use the base ten blocks to model one thousand two hundred seventy-five. Then write the value of each digit."
		}]
	},
	{
		'id':'screen3',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03',
			'captionText':"Use the base ten blocks to model three thousand three hundred thirty-three. Then write the value of each digit."
		}]
	},
	{
		'id':'screen4',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'04',
			'captionText':"Compare the value of the tens digit to the value of the ones digit. Then do the same with the hundreds and the tens digits, and the thousands and hundreds digit. What do you notice?"
		}]
	}
];
