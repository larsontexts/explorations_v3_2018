/* global $ */
/* global ShellModel */
"use strict";

/**
 * Window resize handler.
 */
function onResize() {
  var windowHeight = $(window).height();
  var scrollHeight = windowHeight - 215;
  $(".scrollpane").height(scrollHeight);
}
// Map resize event to calculate new items
$(window).on("resize", onResize);

/**
 * Activity controller.
 * @param {number} slidesCount - the number of steps in the activity.
 * @constructor
 */
function ActivityModel(slidesCount) {
  var ref = this;
  ref.init(slidesCount);
  $(".activity-start-screen .start-btn").on("click", function() {
    $(".activity-start-screen").hide();
    $(".activity").show();
    ref.startActivity();
  });

  this.counterImg = $('#counterImg')[0];
  this.counterSize = 27;

  this.total2 = 1;
  this.rows = 1;
  this.itemsInRow = 1;
  this.questionId = "";

  this.canvas = $("#myCanvas");
  this.ctx = this.canvas[0].getContext("2d");
}
ActivityModel.prototype = new ShellModel();
ActivityModel.prototype.constructor = ActivityModel;

/**
 * Step 1: Initial Screen
 */
ActivityModel.prototype.step1Handler = function() {
  onResize();
  $(".imgcol-sm-6").hide();
  this.removeValidationStyles(); // Clear validation styles.
  this.disableNextButton(false); // Enable the next button.
};

/**
 * Step 2: Drag and make an array with an area of 12.
 */
ActivityModel.prototype.step2Handler = function() {
  var ref = this;

  // Draw the initial counter.
  this.drawCounter(0, this.canvas[0].height - this.counterSize);

  // Touch
  var isTouchDevice = this.isTablet();
  if (isTouchDevice) {
    this.canvas.off("touchstart");
    this.canvas.on("touchstart", this.step2TouchStart.bind(this));
  } else {
    this.canvas.off("mousedown");
    this.canvas.on("mousedown", this.step2MouseDown.bind(this));
  }

  this.canvas.off('touchend mouseup');
  this.canvas.on('touchend mouseup', function(e) {
    e.preventDefault();
    ref.canvas.off("mousemove");
    ref.canvas.off("touchmove");
  });

  $(".imgcol-sm-6").show();

  this.showCheckAnswerButtons(false); // Set button default states.
  this.disableNextButton(true); // Disable the next button.

  /**
   * Handle Checking Answer.
   */
  this.checkBtn
    .off("click")
    .on("click", this.step2CheckAnswer.bind(this));

  // Handle Try Again.
  this.tryBtn
    .off("click")
    .on("click", this.step2TryAgain.bind(this));

  // Handle Show Answer.
  this.showBtn
    .off("click")
    .on("click", this.step2ShowAnswer.bind(this));

  this.canvas.off('keydown').on('keydown', this.step2KeyDown.bind(this));
};

/**
 *
 * @param event
 */
ActivityModel.prototype.step2KeyDown = function(event) {
  if (event.which === 40) { // Left
    this.itemsInRow = (this.itemsInRow > 1) ? (this.itemsInRow - 1) : (1);

  } else if (event.which === 38) { // Right
    this.itemsInRow = (this.itemsInRow < 12) ? (this.itemsInRow + 1) : (12);

  } else if (event.which === 39) { // Up
    this.rows = (this.rows < 12) ? (this.rows + 1) : (12);

  } else if (event.which === 37) { // Down
    this.rows = (this.rows > 1) ? (this.rows - 1) : (1);
  }

  this.total2 = this.rows * this.itemsInRow;
  this.drawArray();
  this.disableButton(this.checkBtn, false);
};

/**
 *
 * @param {object} event - the mouse event
 */
ActivityModel.prototype.step2MouseDown = function(event) {
  var pos = this.getMousePos(this.canvas[0], event);
  var x1 = pos.x;
  var y1 = pos.y;

  // Get the corner.
  var cornerX = this.rows * this.counterSize;
  var cornerY = this.itemsInRow * this.counterSize;

  // If clicked on the draggable circle.
  if (this.intersects(x1, y1, cornerX, this.canvas.height() - cornerY, this.counterSize)) {
    this.canvas.off('mousemove');
    this.canvas.on('mousemove', this.step2MouseMove.bind(this));
  }
};

/**
 *
 * @param {object} event - the mouse event
 */
ActivityModel.prototype.step2MouseMove = function(event) {
  this.ctx.clearRect(0, 0, this.canvas.width(), this.canvas.height());

  var offset = this.canvas.offset();
  var xpos = (event.pageX - offset.left);
  var ypos = this.canvas.height() - (event.pageY - offset.top);
  this.onDrag(xpos, ypos);
};

/**
 *
 * @param {object} event - the touch event
 */
ActivityModel.prototype.step2TouchStart = function(event) {
  event.preventDefault();

  var posTouch = this.getMousePosTouch(this.canvas[0], event);
  var x1Touch = posTouch.x;
  var y1Touch = posTouch.y;

  var t1Touch = 0;
  var t2Touch = 0;
  if (this.total2 > 0) {
    t1Touch = this.rows * this.counterSize;
    t2Touch = this.itemsInRow * this.counterSize;
  }

  if (this.intersects(x1Touch, y1Touch, t1Touch, this.canvas.height() - t2Touch, this.counterSize)) {
    this.canvas.off('touchmove mousemove');
    this.canvas.on('touchmove mousemove', this.step2TouchMove.bind(this));
  }
};

/**
 *
 * @param {object} event - touch event
 */
ActivityModel.prototype.step2TouchMove = function(event) {
  event.preventDefault();
  this.ctx.clearRect(0, 0, this.canvas.width(), this.canvas.height());

  var offset = this.canvas.offset();
  var xPos = (event.touches[0].clientX - offset.left);
  var yPos = this.canvas.height() - (event.touches[0].clientY - offset.top);
  this.onDrag(xPos, yPos);
};

/**
 *
 */
ActivityModel.prototype.step2CheckAnswer = function() {

  // Remove the mouse/touch down events.
  this.canvas
    .off("touchstart")
    .off("mousedown");

  // Remove all questions.
  this.unmarkAllQuestion();

  // Check answer.
  var isCorrect = (this.total2 === 12);
  if (isCorrect) {
    $('.understanding-overlay').addClass('lrn_correct');
    this.showCheckAnswerCorrectButtons();
    this.disableNextButton(false); // Enable the next button.
    this.markQuestion(this.rows, this.itemsInRow);

  } else {
    $('.understanding-overlay').addClass('lrn_incorrect');
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 *
 */
ActivityModel.prototype.step2TryAgain = function() {

  this.showTryAgainButtons(); // update button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  // Add the Mouse/Touch handlers.
  var isTouchDevice = this.isTablet();
  if (isTouchDevice) {
    this.canvas
      .off("touchstart")
      .on("touchstart", this.step2TouchStart.bind(this));
  } else {
    this.canvas
      .off("mousedown")
      .on("mousedown", this.step2MouseDown.bind(this));
  }
};

/**
 *
 */
ActivityModel.prototype.step2ShowAnswer = function() {

  this.removeValidationStyles();
  this.showShowAnswerButtons(); // update the button visibility
  this.disableNextButton(false); // enable the next button.

  // Set the total.
  this.total2 = 12;

  // Update drop area.
  $('.understanding-overlay').addClass('lrn_correct');

  // Redraw with the correct total.
  this.ctx.clearRect(0, 0, this.canvas.width(), this.canvas.height());

  var elem55 = ".activity-slide[data-slide='3'] ";
  $(elem55 + ".learn_4_3").addClass("correct_option");
  this.questionId = '19NA03_c04s01_exp1_int_exploration_1';

  var xx = 0;
  var yy = this.canvas.height() - this.counterSize;

  for (var i = 0; i < 12; i += 1) {
    if (i === 3 || i === 6 || i === 9 || i === 12) {
      yy -= this.counterSize;
      xx = 0;
    }
    this.drawCounter(xx, yy);
    xx += this.counterSize;
  }
};

/**
 * Draws a counter at the given {x, y} coordinate.
 * @param {number} x - the x position
 * @param {number} y - the y position
 */
ActivityModel.prototype.drawCounter = function(x, y) {
  this.ctx.drawImage(this.counterImg, x, y, this.counterSize, this.counterSize);
};

/**
 *
 * @param {number} x - the x position
 * @param {number} y - the y position
 */
ActivityModel.prototype.onDrag = function(x, y) {

  this.rows = (x / this.counterSize).toFixed(0);
  if (this.rows < 1) {
    this.rows = 1;
  } else if (this.rows > 12) {
    this.rows = 12;
  }

  this.itemsInRow = (y / this.counterSize).toFixed(0);
  if (this.itemsInRow < 1) {
    this.itemsInRow = 1;
  } else if (this.itemsInRow > 12) {
    this.itemsInRow = 12;
  }

  // Total area
  this.total2 = this.rows * this.itemsInRow;
  this.drawArray();
  this.disableButton(this.checkBtn, false);
};

/**
 *
 */
ActivityModel.prototype.drawArray = function() {
  this.ctx.clearRect(0, 0, this.canvas.width(), this.canvas.height());

  var c = document.getElementById("myCanvas");
  var xPos;
  var yPos;
  for (var i = 0; i < this.total2; i += 1) {
    xPos = this.counterSize * (i % this.rows);
    yPos = c.height - this.counterSize*(1 + Math.floor(i / this.rows));
    this.drawCounter(xPos, yPos);
  }
};

/**
 *
 */
ActivityModel.prototype.unmarkAllQuestion = function() {
  var elem55 = ".activity-slide[data-slide='3'] ";
  $(elem55 + ".learn_1_12").removeClass("correct_option");
  $(elem55 + ".learn_2_6").removeClass("correct_option");
  $(elem55 + ".learn_3_4").removeClass("correct_option");
  $(elem55 + ".learn_4_3").removeClass("correct_option");
  $(elem55 + ".learn_6_2").removeClass("correct_option");
  $(elem55 + ".learn_12_1").removeClass("correct_option");

};

/**
 *
 * @param {number} rows - the number of rows in the array
 * @param {number} cols - the number of columns in the array
 */
ActivityModel.prototype.markQuestion = function(rows, cols) {
  rows = parseInt(rows, 10);
  cols = parseInt(cols, 10);

  var elem55 = ".activity-slide[data-slide='3'] ";
  if (rows === 1 && cols === 12) {
    $(elem55 + ".learn_12_1").addClass("correct_option");
    this.questionId = '19NA03_c04s01_exp1_int_exploration_12_1';

  } else if (rows === 2 && cols === 6) {
    $(elem55 + ".learn_6_2").addClass("correct_option");
    this.questionId = '19NA03_c04s01_exp1_int_exploration_8';

  } else if (rows === 3 && cols === 4) {
    $(elem55 + ".learn_4_3").addClass("correct_option");
    this.questionId = '19NA03_c04s01_exp1_int_exploration_1';

  } else if (rows === 4 && cols === 3) {
    $(elem55 + ".learn_3_4").addClass("correct_option");
    this.questionId = '19NA03_c04s01_exp1_int_exploration_4';

  } else if (rows === 6 && cols === 2) {
    $(elem55 + ".learn_2_6").addClass("correct_option");
    this.questionId = '19NA03_c04s01_exp1_int_exploration_6';

  } else if (rows === 12 && cols === 1) {
    $(elem55 + ".learn_1_12").addClass("correct_option");
    this.questionId = '19NA03_c04s01_exp1_int_exploration_1_12';
  }
};

/**
 *
 * @param {object} canvas - the canvas element
 * @param {object} event - the mouse event
 * @return {{x: number, y: number}} the point relative to the mouse.
 */
ActivityModel.prototype.getMousePos = function(canvas, event) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top
  };
};

/**
 *
 * @param {canvas} canvas the canvas element
 * @param {object} event - the touch event
 * @return {{x: number, y: number}} the point relative to the canvas
 */
ActivityModel.prototype.getMousePosTouch = function(canvas, event) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: event.touches[0].clientX - rect.left,
    y: event.touches[0].clientY - rect.top
  };
};

/**
 *
 * @param {number} x - x pos
 * @param {number} y - y pos
 * @param {number} cx - center x
 * @param {number} cy - center y
 * @param {number} r - radius
 * @return {boolean} whether the point {x,y} is within radius distance from {cx, cy}
 */
ActivityModel.prototype.intersects = function(x, y, cx, cy, r) {
  var dx = x - cx;
  var dy = y - cy;
  return dx * dx + dy * dy <= r * r;
};

/**
 * Step 3: Enter the values for rows and columns.
 */
ActivityModel.prototype.step3Handler = function() {
  // Remove the answer style from the canvas.
  this.removeValidationStyles();

  // Based on rows and items in rows decide what to view.
  $(".learn_1_12").hide();
  $(".learn_2_6").hide();
  $(".learn_3_4").hide();
  $(".learn_4_3").hide();
  $(".learn_6_2").hide();
  $(".learn_12_1").hide();

  var elem55 = ".activity-slide[data-slide='3'] ";
  $(elem55 + ".correct_option").show();
  $(elem55 + ".correct_option .answer").hide();
  $(elem55 + ".correct_option .question").show();
  $(".lrn .lrn_btn.lrn_validate>span").text("Check");
  $(".dd").css("display", "block");

  var checkBtn = $('#check_btn2');
  var tryBtn = $('#try_btn2');
  var showBtn = $('#show_btn2');

  // Hide the check button.
  var learnCheck = $(".correct_option .lrn_validate").eq(0);
  learnCheck.hide();

  this.showCheckAnswerButtons(true); // Update button visibility.
  this.disableNextButton(true); // Disable the Next button.
  this.enableQuestion(this.questionId); // Enable the learnosity question.

  // Check handler.
  checkBtn
    .off('click')
    .on('click', this.step3CheckAnswer.bind(this));

  // Try handler.
  tryBtn
    .off('click')
    .on('click', this.step3TryAgain.bind(this));

  // Show answer handler.
  showBtn
    .off('click')
    .on('click', this.step3ShowAnswer.bind(this));
};

/**
 *
 */
ActivityModel.prototype.step3CheckAnswer = function() {
  // Learnosity API check.
  var isCorrect = this.checkAnswer(this.questionId);
  if (isCorrect) {
    this.showCheckAnswerCorrectButtons(); // Hide the check button.
    this.disableNextButton(false); // Enable the next button.

  } else {
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 *
 */
ActivityModel.prototype.step3TryAgain = function() {
  this.enableQuestion(this.questionId);
  this.showTryAgainButtons(true);
  this.removeValidationStyles(); // Remove all (in)correct styles.
};

/**
 *
 */
ActivityModel.prototype.step3ShowAnswer = function() {
  this.removeValidationStyles();
  this.showShowAnswerButtons();
  this.disableNextButton(false); // Enable the Next button.

  var elem55 = ".activity-slide[data-slide='3'] ";
  $(elem55 + ".correct_option .question").hide();
  $(elem55 + ".correct_option .answer")
    .addClass('big-correct')
    .show();
};

/**
 * Step 4: Essay answer.
 */
ActivityModel.prototype.step4Handler = function() {
  // Animate scrolling to the bottom
  var scrollPanel = $(".scrollpane");
  var animateTime = 1000; // 1 second
  scrollPanel.animate({scrollTop: scrollPanel.prop("scrollHeight")}, animateTime);

  //
  $(".dd").css("display", "block");

  var elem55 = ".activity-slide[data-slide='3'] ";
  $(elem55 + ".correct_option .question").hide();
  $(elem55 + ".correct_option .answer").show();

  var elem2 = ".activity-slide[data-slide='2'] ";
  $(elem2 + '.drop-area2 .drop-area-block2').droppable("disable"); // Disable the canvas.

  this.removeValidationStyles(); // Remove the canvas validation style.
  this.disableDoneButton(true); // Disable the done button.
  this.enableQuestion('19NA03_c04s01_exp1_int_exploration_3'); // Enable essay input.

  // Add the click save handler.
  var elem = ".activity-slide[data-slide='4'] ";
  var saveBtn = $(elem + ".btn-save");
  this.initSaveButton(saveBtn, this.step4SaveHandler.bind(this));
};

/**
 * Step 4: Save handler.
 * Save responses to Learnosity.
 */
ActivityModel.prototype.step4SaveHandler = function() {
  this.disableDoneButton(false); // Enable the done button.
  this.disableQuestion('19NA03_c04s01_exp1_int_exploration_3'); // Disable essay input.
};

/**
 * Step 5: End screen.
 */
ActivityModel.prototype.step5Handler = function() {
  this.removeValidationStyles(); // Remove validation styles.

  $(".dd").css("display", "block");
  var elemD = ".activity-slide[data-slide='3'] ";
  $(elemD + '.drop-area3 .drop-area-block3').droppable("disable");
};

/**
 * Reset activity.
 */
ActivityModel.prototype.resetActivityHandler = function() {

  this.disableNextButton(false); // Enable the next button.
  this.disableDoneButton(true); // Disable the done button.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  // Reset the properties
  this.rows = 1;
  this.itemsInRow = 1;
  this.total2 = 1;

  for (var i = 1; i <= this.curSlide; i++) {
    var elem = ".activity-slide[data-slide='" + i + "'] ";
    if ($(elem + " .draggable-item").hasClass("ui-draggable")) {
      $(elem + " .draggable-item").draggable("destroy");
    }
    if ($(elem + " .drop-area" + i + " .drop-area-block" + i).hasClass("ui-droppable")) {
      $(elem + " .drop-area" + i + " .drop-area-block" + i).droppable("destroy");
    }
  }
  $(".activity .drop-area .drop-area-block .draggable-item").remove();

  var selectList = $(".drag");
  selectList.find("div:gt(0)").remove();

  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");
  ctx.clearRect(0, 0, c.width, c.height);

  var elem55 = ".activity-slide[data-slide='3'] ";
  $(elem55 + ".correct_option").hide();
  $(elem55 + ".correct_option .question").hide();
  $(elem55 + ".correct_option .answer").hide();

  $(elem55 + ".learn_1_12").hide();
  $(elem55 + ".learn_2_6").hide();
  $(elem55 + ".learn_3_4").hide();
  $(elem55 + ".learn_4_3").hide();
  $(elem55 + ".learn_6_2").hide();
  $(elem55 + ".learn_12_1").hide();

  $(".correct_option").removeClass("correct_option");
};
