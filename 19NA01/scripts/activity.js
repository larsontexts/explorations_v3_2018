/* global $ */
/* global ShellModel */
"use strict";

/**
 *
 * @param slidesCount
 * @constructor
 */
function ActivityModel(slidesCount) {
  var ref = this;
  ref.init(slidesCount);
  $('.activity-start-screen .start-btn').click(function() {
    $('.activity-start-screen').hide();
    $('.activity').show();
    ref.startActivity();
  });

  this.dogCorrectAmount = 5;
  this.catCorrectAmount = 3;
}
ActivityModel.prototype = new ShellModel();
ActivityModel.prototype.constructor = ActivityModel;

/**
 * Step 1: Handler.
 */
ActivityModel.prototype.step1Handler = function() {
  var ref = this;
  ref.disableNextButton(false); // Enable the next button.
};

/**
 * Step 2: Handler.
 */
ActivityModel.prototype.step2Handler = function() {
  var ref = this;

  // Init variables.
  var elem = ".activity-slide[data-slide='2'] ";
  var checkBtn = $(elem + '#check_btn');
  var tryBtn = $(elem + '#try_btn');
  var showBtn = $(elem + '#show_btn');

  $(elem + ' .draggable-item').draggable({
    revert: 'invalid',
    stack: '.draggable-item',
    helper: 'clone',
    zIndex: 10001,
    containment: '.activity-content-area',
    scope: 'slide-1'
  });

  $(elem + ' .draggable-item').show();
  $(elem + ' .draggable-item').draggable('enable');

  // DropArea 2
  var outside = 0;
  $(elem + '.drop-area2').droppable({
    scope: 'slide-1',
    accept: function(draggable) {
      if ($(this).find('.draggable-item').length < 7) {
        return true;
      } else {
        return false;
      }
    },
    out: function() {
      outside = 1;
    },
    over: function() {
      outside = 0;
    },
    drop: function(event, ui) {
      var droppable = $(event.target);
      var draggable = ui.draggable;

      var d;
      if (draggable.parent().hasClass('drop-area2')) {
        d = draggable.detach();
      } else {
        d = draggable.clone();
      }

      ref.step2AddToDropArea(d, droppable, outside, 'nineClass');
    }
  });

  $(elem + ' .draggable-items .draggable-item').draggable('enable');
  $(elem + ' .drop-area2').droppable('enable');
  $(elem + ' .drop-area2').droppable('enable');

  // DropArea 1
  var outsideArea1 = 0;
  $(elem + '.drop-area1').droppable({
    scope: 'slide-1',
    accept: function(draggable) {
      if ($(this).find('.draggable-item').length < 7) {
        return true;
      } else {
        return false;
      }
    },
    out: function() {
      outsideArea1 = 1;
    },
    over: function() {
      outsideArea1 = 0;
    },
    drop: function(event, ui) {
      var droppable = $(event.target);
      var draggable = ui.draggable;

      var d;
      if (draggable.parent().hasClass('drop-area1')) {
        d = draggable.detach();
      } else {
        d = draggable.clone();
      }

      ref.step2AddToDropArea(d, droppable, outsideArea1, 'sixClass');
    }
  });

  $(elem + ' .draggable-items .draggable-item').draggable('enable');
  $(elem + ' .drop-area1').droppable('enable');
  $(elem + ' .drop-area1 .draggable-item').droppable('enable');

  this.showCheckAnswerButtons(); // Set button default states.
  this.disableNextButton(true); // Disable the next button.

  // Add click handlers.
  checkBtn.on('click', this.step2CheckAnswer.bind(this));
  tryBtn.on('click', this.step2TryAgain.bind(this));
  showBtn.on('click', this.step2ShowAnswer.bind(this));

  // Add keyboard handlers.
  var dragItem = $(elem + '.draggable-items .draggable-item');
  var dropArea2 = $(elem + '.drop-area2');
  var dropArea1 = $(elem + '.drop-area1');
  dropArea2
    .off('keydown')
    .on('keydown', function(event) {
      if (event.which === 13 || event.which === 32) {
        ref.step2AddToDropArea(dragItem.clone(), dropArea2, outside, 'nineClass');
      }
    });
  dropArea1
    .off('keydown')
    .on('keydown', function(event) {
      if (event.which === 13 || event.which === 32) {
        ref.step2AddToDropArea(dragItem.clone(), dropArea1, outsideArea1, 'sixClass');
      }
    });
};

ActivityModel.prototype.step2AddToDropArea = function(d, droppable, outside, classname) {
  var ref = this;

  d.css({left: 'auto', top: 'auto'});
  d.addClass(classname);

  // Prevent overflowing counters.
  if (droppable[0].childElementCount >= 7) {
    return;
  }

  d.appendTo(droppable);

  $(d).draggable({
    stack: '.draggable-item',
    zIndex: 10001,
    containment: '.activity .activity-content-area',
    scope: 'slide-1',
    revert: function(draggable) {

      if ((outside === 1 || draggable === false) && !(d.hasClass(classname))) {
        $(this).fadeOut(400, function() {
          $(this).remove();
          ref.step2UpdateCheckButton(); // Check if drop area has children
        });
        return false;

      } else if ($(draggable).hasClass('.draggable-item')) {
        return false;

      } else if (d.hasClass(classname)) {
        $(this).remove();
        ref.step2UpdateCheckButton(); // Check if drop area has children

      } else {
        return true;
      }
    }
  });

  d.off('keydown').on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      event.preventDefault();
      event.stopPropagation();
      d.draggable("destroy");
      d.remove();
      ref.step2UpdateCheckButton();
    }
  });

  // check if drop area has children
  this.step2UpdateCheckButton();
};

/**
 * Step 2: Update Check Button.
 */
ActivityModel.prototype.step2UpdateCheckButton = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  if ($(elem + '.drop-area1').children().length >= 1 ||
      $(elem + '.drop-area2').children().length >= 1) {
    this.checkBtn.prop('disabled', false);
    this.checkBtn.removeClass('disabled');

  } else {
    this.checkBtn.prop('disabled', true);
    this.checkBtn.addClass('disabled');
  }
};

/**
 * Step 2: Is Correct.
 * @return {boolean} whether the answer is correct or not.
 */
ActivityModel.prototype.step2IsCorrect = function() {
  var dropArea = $('.drop-area1');
  var dropArea2 = $('.drop-area2');
  var isCorrect = (dropArea[0].childElementCount === this.dogCorrectAmount) &&
                  (dropArea2[0].childElementCount === this.catCorrectAmount);
  return isCorrect;
};

/**
 * Step 2: Check Answer handler.
 */
ActivityModel.prototype.step2CheckAnswer = function() {
  // Init variables.
  var elem = ".activity-slide[data-slide='2'] ";
  var understandingOverlay = $(elem + '.understanding-overlay');

  // Disable dragging.
  $(elem + '.draggable-items .draggable-item').draggable('disable').hide();
  $('.drop-area1 .draggable-item').draggable('disable');
  $('.drop-area2 .draggable-item').draggable('disable');

  // Check answer.
  var isCorrect = this.step2IsCorrect();
  if (isCorrect) {
    this.showCheckAnswerCorrectButtons(); // Update button visibility.
    this.disableNextButton(false); // Enable the next button.
    understandingOverlay.addClass('correct'); // Update overlay.

    // Disable key handlers.
    $('.drop-area1').off('keydown');
    $('.drop-area2').off('keydown');
    $('.draggable-item').off('keydown');

  } else {
    this.showCheckAnswerIncorrectButtons(); // Update button visibility.
    understandingOverlay.addClass('incorrect'); // Update overlay.
  }
};

/**
 * Step 2: Try Again handler.
 */
ActivityModel.prototype.step2TryAgain = function() {
  this.removeValidationStyles(); // Remove correct and incorrect styles
  this.showTryAgainButtons(); // Update button visibility.

  // Init variables.
  var elem = ".activity-slide[data-slide='2'] ";
  var dragItem = $(elem + '.draggable-item');

  // Re-enable the dragging
  dragItem.draggable().draggable('enable').show();
  $('.drop-area1 .draggable-item').draggable('enable');
  $('.drop-area2 .draggable-item').draggable('enable');

  // check if drop area has children
  if ($(elem + '.drop-area1').children().length >= 1 ||
      $(elem + '.drop-area2').children().length >= 1) {
    this.checkBtn.prop('disabled', false);
    this.checkBtn.removeClass('disabled');
  }
};

/**
 * Step 2: Show Answer handler.
 */
ActivityModel.prototype.step2ShowAnswer = function() {
  this.removeValidationStyles(); // Remove validation styles.
  this.showShowAnswerButtons(); // Update the button visibility.
  this.disableNextButton(false); // Enable the next button.

  // Init variables.
  var elem = ".activity-slide[data-slide='2'] ";
  var dropArea = $('.drop-area1');
  var dropArea2 = $('.drop-area2');
  var understandingOverlay = $(elem + '.understanding-overlay');
  var dragItem = $(elem + '.draggable-items .draggable-item');
  var i;

  // Disable key handlers.
  $('.drop-area1').off('keydown');
  $('.drop-area2').off('keydown');
  $('.draggable-item').off('keydown');

  understandingOverlay.addClass('correct'); // Update overlay.

  // Add the proper amount to the drop area.
  dropArea.empty(); // Remove all items.
  var item = dragItem.clone();
  item.show();
  for (i = 0; i < this.dogCorrectAmount; i += 1) {
    item.clone().appendTo(dropArea);
  }

  // Add the proper amount to the drop area.
  dropArea2.empty(); // Remove all items.
  item.show();
  for (i = 0; i < this.catCorrectAmount; i += 1) {
    item.clone().appendTo(dropArea2);
  }
};

/**
 * Step 3: Handler.
 */
ActivityModel.prototype.step3Handler = function() {
  var elem = ".activity-slide[data-slide='3'] ";

  // Init variables.
  var checkBtn = $(elem + '#check_btn3');
  var tryBtn = $(elem + '#try_btn3');
  var showBtn = $(elem + '#show_btn3');

  $(elem + '.question1').show(); // Show the question.
  $(elem + '.answer1').hide(); // Hide the answer.
  $('.lrn .lrn_btn.lrn_validate').hide(); // hide learnosity check button.

  // Update buttons.
  checkBtn.show();
  tryBtn.hide();
  showBtn.hide();

  this.disableNextButton(true); // disable next button.
  this.removeValidationStyles(); // Remove all (in)correct styles.
  this.enableQuestion('19NA01_c03s05_exp1_int_exploration_1'); // Enable the inputs.

  // Add click handlers.
  checkBtn.off('click').on('click', this.step3CheckAnswer.bind(this));
  tryBtn.off('click').on('click', this.step3TryAgain.bind(this));
  showBtn.off('click').on('click', this.step3ShowAnswer.bind(this));
};

/**
 * Step 3: Check Answer handler.
 */
ActivityModel.prototype.step3CheckAnswer = function() {

  // Use ItemApi + Question Method to run validation on the question.
  var isCorrect = this.checkAnswer('19NA01_c03s05_exp1_int_exploration_1');
  if (isCorrect) {
    this.disableNextButton(false); // Enable 'Next' button.

    // Update button visibility.
    this.checkBtn.hide(); // Toggle button visibility.
    this.tryBtn.hide();
    this.showBtn.hide();

    var elem = ".activity-slide[data-slide='3'] ";
    var understandingOverlay = $(elem + '.understanding-overlay');

    $(elem + '.question1').hide(); // Hide the question.
    $(elem + '.answer1').show(); // Show the answer.
    understandingOverlay.addClass('correct');

  } else {
    // Update button visibility.
    this.checkBtn.hide(); // Toggle button visibility.
    this.tryBtn.show();
    this.showBtn.show();
  }
};

/**
 * Step 3: Try Again handler.
 */
ActivityModel.prototype.step3TryAgain = function() {
  this.removeValidationStyles(); // Remove all (in)correct styles.

  // Update button visibility.
  this.checkBtn.show();
  this.tryBtn.hide();
  this.showBtn.hide();

  this.enableQuestion('19NA01_c03s05_exp1_int_exploration_1');
};

/**
 * Step 3: Show Answer handler.
 */
ActivityModel.prototype.step3ShowAnswer = function() {
  this.removeValidationStyles();
  this.disableNextButton(false); // Enable 'Next' button.
  this.showShowAnswerButtons(); // Hide the buttons

  // Init variables.
  var elem = ".activity-slide[data-slide='3'] ";
  var understandingOverlay = $(elem + '.understanding-overlay');

  $(elem + '.question1').hide(); // Hide the question.
  $(elem + '.answer1').show(); // Show the answer.
  understandingOverlay.addClass('correct'); // Add correct style.
};

/**
 * Step 4: Handler.
 */
ActivityModel.prototype.step4Handler = function() {
  this.removeValidationStyles(); // Remove all (in)correct styles.
  this.disableDoneButton(true);

  // Add a handler to listen to the audio player.
  var audioPlayer = $(".activity-audio[data-slide='4']")[0];
  this.audioHandler = audioPlayer.addEventListener('ended', this.waitForAudio.bind(this));
};

/**
 * Step 5: End Screen handler.
 */
ActivityModel.prototype.step5Handler = function() {
  // End Screen.
};

/**
 * Reset handler.
 */
ActivityModel.prototype.resetActivityHandler = function() {
  this.disableNextButton(false); // Enable the next button.
  this.disableDoneButton(true); // Disable the done button.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  $('.activity .drop-area  .draggable-item').remove();
  $(".activity-slide[data-slide='2'] .draggable-items").show();
  $('.activity').find('.activity-question-option').removeClass('selected');

  // Clean-up events.
  if (this.audioHandler !== null) {
    var audioPlayer = $(".activity-audio[data-slide='4']")[0];
    audioPlayer.removeEventListener('ended', this.audioHandler);
  }
};

/**
 * Wait until the audio has finished then enable the audio.
 */
ActivityModel.prototype.waitForAudio = function() {
  this.disableDoneButton(false);
};

