/*Jouve created for key board event*/
var bool = false;
var count = 0;
var pg_count = 0;
var flag_rev = false;
$(document).ready(function() {
    var flag_lag = false;
    $('a').first().trigger('focus');
    $(window).keyup(function(ee) {
        if (ee.target.className == 'option active') {
            flag_lag = true;
        }
    });
    $('.option').keydown(function(ee) {
        if ($(this).text() == 'Espanol' && ee.keyCode == 9 && !ee.shiftKey) {
            $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
            count = 0;
        } else if ($(this).text() == 'English' && ee.keyCode == 9 && ee.shiftKey) {
            $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
            count = 0;
        }
    });
    $('.language-toggle').keypress(function(eee) {
        if ((eee.keyCode == 13 && flag_lag == true) || (eee.keyCode == 32 && flag_lag == true)) {
            $(".activity .activity-navigation .language-toggle").removeClass("closed").addClass("open");
            $('.language-toggle span').each(function(eeee) {
                bool = $(this).hasClass("active");
                if ($(this)["0"].attributes["0"].value == eee.target.attributes["0"].value) {
                    $('.option').removeClass('active');
                    $(this).addClass('active');
                    if ((bool == true) && (($(this).text() == "English") || ($(this).text() == "Espanol"))) {
                        if (count == 0) {
                            $(".activity .activity-navigation .language-toggle").removeClass("closed").addClass("open");
                            count = 1;
                        } else {
                            $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
                            count = 0;
                        }

                    } else {
                        count = 0;
                        $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
                    }
                }
            });
            flag_lag = false;
        }
    });
});

// ActivityModel.prototype.step3Handler = function() {
//     var ref = this;
//     var elem = ".activity-slide[data-slide='3'] ";
//     $(elem).find(".activity-question-option").off("keypress").on("keypress", function() {
//         if ($(this).hasClass("disabled")) return;
//         $(elem).find(".activity-question-option").removeClass("selected");
//         $(this).addClass("selected");
//         $(elem).find(".check-answer").text("Check Answer");
//         $(elem).find(".correct-answer").hide();
//         $(elem).find(".wrong-answer").hide();
//     });
// }
// ActivityModel.prototype.step4Handler = function() {
//     var ref = this;
//     var elem = ".activity-slide[data-slide='4'] ";
//     $(elem).find(".activity-question-option").off("keypress").on("keypress", function() {
//         if ($(this).hasClass("disabled")) return;
//         $(elem).find(".activity-question-option").removeClass("selected");
//         $(this).addClass("selected");
//         $(elem).find(".check-answer").text("Check Answer");
//         $(elem).find(".correct-answer").hide();
//         $(elem).find(".wrong-answer").hide();
//     });
// }