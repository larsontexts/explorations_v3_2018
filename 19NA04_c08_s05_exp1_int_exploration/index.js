var gradeChapter = "19NA04_c08";
var chapterSection = "s05";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{	
		'id':'screen1', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01', 
			'captionText':"Write each fraction as a sum of unit fractions. Use models to help."
		}]
	},
	{
		'id':'screen2', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02', 
			'captionText':"Write four fifths and three fifths as a sum of unit fractions. Shade the models to help."
		}]
	},
	{	
		'id':'screen3', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03', 
			'captionText':"How many more unit fractions did you use to rewrite four-fifths than three-fifths?\n How does this relate to the difference four-fifths minus three-fifths?"
		}]
	},
	{	
		'id':'screen4', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'04', 
			'captionText':"How can you use the numerators and the denominators to subtract fractions with like denominators? Explain why your method makes sense."
		}]
	}
];