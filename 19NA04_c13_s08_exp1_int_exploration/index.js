var gradeChapter = "19NA04_c09";
var chapterSection = "s04";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{	
		'id':'screen1', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01', 
			'captionText':"Use models to help you complete the table."
		}]
	},
	{
		'id':'screen2', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02', 
			'captionText':"Find each product to complete the table"
		}]
	},
	{	
		'id':'screen3', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03', 
			'captionText':"How does the Distributive Property relate to your models? Explain."
		}]
	}
];