var gradeChapter = "19NA04_c05";
var chapterSection = "s09";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{	
		'id':'screen1', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01', 
			'captionText':"Make a plan to solve the problem.\n A fruit vendor has three hundred fifty two green apples and four hundred twenty-four red apples. The vendor uses all of the apples to make fruit baskets. He puts eight apples into each basket. How many fruit baskets does the vendor make?"
		}]
	},
	{
		'id':'screen2', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02', 
			'captionText':"Make a plan to solve the problem. \n Then write your answer in the box"
		}]
	}
	/*,
	{	
		'id':'screen3', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03', 
			'captionText':"Make Sense of Problems. The vendor decides that each basket should have eight of the same colored apples. Does this change your plan to solve the problem? Will this change the answer? Explain."
		}
	*/
	
	
];