/* global $ */
/* global ShellModel */
"use strict";

/**
 * Activity controller.
 * @param {number} slidesCount - the number of steps in the activity.
 * @constructor
 */
function ActivityModel(slidesCount) {
  var ref = this;
  ref.init(slidesCount);
  $(".activity-start-screen .start-btn").on("click", function() {
    $(".activity-start-screen").hide();
    $(".activity").show();
    ref.startActivity();
  });
}
ActivityModel.prototype = new ShellModel();
ActivityModel.prototype.constructor = ActivityModel;

/**
 * Step 1: Initial screen.
 */
ActivityModel.prototype.step1Handler = function() {
  this.disableNextButton(false); // Enable the next button.
  this.removeValidationStyles(); // Remove all styles.
};

/**
 * Step 2: Dragging yellow cubes.
 */
ActivityModel.prototype.step2Handler = function() {
  var ref = this;
  var elem = ".activity-slide[data-slide='2'] ";
  var elemD = ".activity-slide[data-slide='1'] ";

  $(elemD+'.drop-area1 .drop-area-block1').droppable("disable");

  // Init variables.
  var checkBtn = $(elem+'#check_btn');
  var tryBtn = $(elem+'#try_btn');
  var showBtn = $(elem+'#show_btn');
  var dropArea = $(elem+'.drop-area2 .drop-area-block2');
  var dragItem = $(elem+'.draggable-item');

  // Setup keyboard accessability.
  dragItem.off('keydown');
  dragItem.on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      ref.addToDropArea($(this).clone(), dropArea);
    }
  });

  // Setup dragging.
  dragItem.draggable();
  dragItem.draggable({
    revert: "invalid",
    zIndex: 10001,
    helper: 'clone',
    containment: '.activity-content-area'
  });
  dragItem.draggable('enable');
  dragItem.show();

  // Setup dropping.
  dropArea.droppable({
    accept: ".draggable-item",
    over: function(event, ui) {
      ui.helper.addClass('hover');
    },
    out: function(event, ui) {
      ui.helper.removeClass('hover');
    },
    drop: function(event, ui) {
      var draggable = ui.draggable;
      var d = draggable;
      if (ui.draggable !== ui.helper) {
        d = draggable.clone();
      }
      ref.addToDropArea(d, dropArea);
    }
  });
  dropArea.droppable("enable");
  dropArea.empty(); // Clear contents on load.

  this.showCheckAnswerButtons(); // Set button default states.
  this.disableNextButton(true); // Disable the next button.

  // Handle Checking Answer.
  checkBtn.off("click");
  checkBtn.on("click", this.step2CheckAnswer.bind(this));

  // Try again handler.
  tryBtn.off("click");
  tryBtn.on("click", this.step2TryAgain.bind(this));

  // Show answer handler.
  showBtn.off("click");
  showBtn.on("click", this.step2ShowAnswer.bind(this));
};

/**
 * Step 2: Check Answer handler.
 */
ActivityModel.prototype.step2CheckAnswer = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var understandingOverlay = $(elem+'.understanding-overlay');
  var dropArea = $(elem+'.drop-area2 .drop-area-block2');
  var dragItems = $(elem+'.draggable-item');

  // Hide the drag blocks.
  var dragBlockItems = $(elem+'.draggable-items .draggable-item');
  dragBlockItems.hide();

  // Disable dragging
  dragItems.draggable('disable');
  $('.drop-area-block2 .placed').draggable('disable');

  // Check answer.
  var isCorrect = dropArea[0].childElementCount === 7;
  if (isCorrect) {
    understandingOverlay.addClass('lrn_correct');
    this.showCheckAnswerCorrectButtons(); // Update button visibility
    this.disableNextButton(false); // Enable the next button.

  } else {
    understandingOverlay.addClass('lrn_incorrect');
    this.showCheckAnswerIncorrectButtons(); // Update button visibility
  }
};

/**
 * Step 2: Try Again handler.
 */
ActivityModel.prototype.step2TryAgain = function() {
  var elem = ".activity-slide[data-slide='2'] ";

  // Re-enable the dragging.
  var dragItem = $(elem+'.draggable-item');
  dragItem.draggable().draggable('enable');
  dragItem.show();

  this.showTryAgainButtons(); // Update button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles.
};

/**
 * Step 2: Show Answer handler.
 */
ActivityModel.prototype.step2ShowAnswer = function() {
  this.showShowAnswerButtons(); // Update the button visibility.
  this.disableNextButton(false); // Enable the next button.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  var elem = ".activity-slide[data-slide='2'] ";

  // Update drop area.
  var understandingOverlay = $(elem+'.understanding-overlay');
  understandingOverlay.removeClass('lrn_incorrect');
  understandingOverlay.addClass('lrn_correct');

  // Remove all items.
  var dropArea = $(elem+'.drop-area2 .drop-area-block2');
  dropArea.empty();

  // Add the proper amount to the drop area.
  var correctAmount = 7;
  var dragItem = $(elem+'.draggable-items .draggable-item');
  var item = dragItem.clone();
  item.show();
  for (var i=0; i<correctAmount; i+=1) {
    item.clone().appendTo(dropArea);
  }
};

/**
 * Updates the 'Check' button's enabled state.
 */
ActivityModel.prototype.updateCheck1 = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var dropArea = $(elem+'.drop-area2 .drop-area-block2');
  var checkBtn = $(elem+'#check_btn');

  if (dropArea[0].childElementCount > 0) {
    checkBtn.prop('disabled', false);
    checkBtn.removeClass("disabled");
  } else {
    checkBtn.prop('disabled', true);
    checkBtn.addClass("disabled");
  }
};

/**
 * Add a counter to the drop area.
 * @param {object} $draggable - the counter
 * @param {object} $dropArea - the drop area.
 * @param {number} x - the x position.
 * @param {number} y - the y position.
 */
ActivityModel.prototype.addToDropArea = function($draggable, $dropArea, x, y) {
  var ref = this;

  if ($dropArea[0].childElementCount >= 10) {
    return;
  }

  $draggable.removeClass('ui-draggable-dragging');
  $draggable.appendTo($dropArea);

  ref.updateCheck1();

  // Limit to only 10 items.
  if ($dropArea[0].childElementCount >= 10) {
    $dropArea.droppable("disable");
  }

  var removeDraggable = function() {
    $draggable.draggable("destroy");
    $draggable.remove();
    ref.updateCheck1();

    // Limit to only 10 items.
    if ($dropArea[0].childElementCount < 10) {
      $dropArea.droppable("enable");
    }
  };

  // Make this object draggable.
  $draggable.draggable({
    revert: "false",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: "original",
    containment: '.activity-content-area',
    stop: function() {
      if (!$draggable.hasClass('placed')) {
        removeDraggable();
      }
    }
  });

  // Add accessibility handler.
  $draggable.off('keydown');
  $draggable.on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      removeDraggable();
    }
  });
};

/**
 * Step 3: Dragging red cubes.
 */
ActivityModel.prototype.step3Handler = function() {
  var ref = this;

  // Old slide clean-up.
  var elemD = ".activity-slide[data-slide='2'] ";
  $(elemD+'.drop-area2 .drop-area-block2').droppable("disable");
  $(elemD+'.understanding-overlay').removeClass('lrn_correct');
  $(elemD+'.understanding-overlay').removeClass('lrn_incorrect');

  // Init variables
  var elem = ".activity-slide[data-slide='3'] ";
  var checkBtn = $(elem+'#check_btn_2');
  var tryBtn = $(elem+'#try_btn_2');
  var showBtn = $(elem+'#show_btn_2');
  var dropArea = $(elem+'.drop-area4 .drop-area-block4');
  var dragItem = $(elem+'.draggable-item');

  // Setup keyboard accessability.
  dragItem.off('keydown');
  dragItem.on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      ref.addToDropArea2($(this).clone(), dropArea);
    }
  });

  // Setup dragging
  dragItem.draggable();
  dragItem.draggable({
    revert: "invalid",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: 'clone',
    containment: '.activity-content-area'
  });
  dragItem.draggable('enable');
  dragItem.show();

  dropArea.droppable({
    accept: ".draggable-item",
    over: function(event, ui) {
      ui.helper.addClass('hover');
    },
    out: function(event, ui) {
      ui.helper.removeClass('hover');
    },
    drop: function(event, ui) {
      var draggable = ui.draggable;
      var d = draggable;
      if (ui.draggable !== ui.helper) {
        d = draggable.clone();
      }
      ref.addToDropArea2(d, dropArea);
    }
  });
  dropArea.droppable("enable");

  // Set button default states
  this.showCheckAnswerButtons();

  // Disable the next button.
  this.disableNextButton(true);

  // Handle Checking Answer.
  checkBtn.off("click");
  checkBtn.on("click", this.step3CheckAnswer.bind(this));

  // Handle Try Again.
  tryBtn.off("click");
  tryBtn.on("click", this.step3TryAgain.bind(this));

  // Handle Show Answer.
  showBtn.off("click");
  showBtn.on("click", this.step3ShowAnswer.bind(this));
};

/**
 * Step 3: Check Answer.
 */
ActivityModel.prototype.step3CheckAnswer = function() {
  var elem = ".activity-slide[data-slide='3'] ";
  var dropArea = $(elem+'.drop-area4 .drop-area-block4');
  var understandingOverlay = $(elem+'.understanding-overlay');
  var dragItem = $(elem+'.draggable-items .draggable-item');

  // disable dragging
  dragItem.hide();
  dragItem.draggable('disable');
  $('.drop-area-block4 .placed').draggable('disable');

  // Check answer.
  var isCorrect = dropArea[0].childElementCount === 5;
  if (isCorrect) {
    // update overlay
    understandingOverlay.addClass('lrn_correct');
    this.showCheckAnswerCorrectButtons(); // Update button visibility.
    this.disableNextButton(false); // Enable the next button.

  } else {
    // update overlay
    understandingOverlay.addClass('lrn_incorrect');
    this.showCheckAnswerIncorrectButtons(); // Update button visibility.
  }
};

/**
 * Step 3: Try Again handler.
 */
ActivityModel.prototype.step3TryAgain = function() {
  this.showTryAgainButtons(); // Update button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles

  // Re-enable the dragging
  var elem = ".activity-slide[data-slide='3'] ";
  var dragItem = $(elem+'.draggable-item');
  dragItem.draggable().draggable('enable');
  dragItem.show();
};

/**
 * Step 3: Show Answer handler.
 */
ActivityModel.prototype.step3ShowAnswer = function() {
  this.showShowAnswerButtons(); // Update the button visibility.
  this.disableNextButton(false); // Enable the next button.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  var elem = ".activity-slide[data-slide='3'] ";
  var dropArea = $(elem+'.drop-area4 .drop-area-block4');
  var understandingOverlay = $(elem+'.understanding-overlay');
  var dragItem = $(elem+'.draggable-items .draggable-item');

  // Update overlay.
  understandingOverlay.addClass('lrn_correct');

  // Remove all items.
  dropArea.empty();

  // add the proper amount to the drop area.
  var correctAmount = 5;
  var item = dragItem.clone();
  item.show();
  for (var i=0; i<correctAmount; i+=1) {
    item.clone().appendTo(dropArea);
  }
};

/**
 * Updates the 'Check' button's enabled state.
 */
ActivityModel.prototype.updateCheck2 = function() {
  var elem = ".activity-slide[data-slide='3'] ";
  var dropArea = $(elem+'.drop-area4 .drop-area-block4');
  var checkBtn = $(elem+'#check_btn_2');

  if (dropArea[0].childElementCount > 0) {
    checkBtn.prop('disabled', false);
    checkBtn.removeClass("disabled");
  } else {
    checkBtn.prop('disabled', true);
    checkBtn.addClass("disabled");
  }
};

/**
 * Add a counter to the drop area.
 * @param {object} $draggable - the counter
 * @param {object} $dropArea - the drop area
 * @param {number} x - the x position.
 * @param {number} y - the y position.
 */
ActivityModel.prototype.addToDropArea2 = function($draggable, $dropArea, x, y) {
  var ref = this;

  if ($dropArea[0].childElementCount >= 10) {
    return;
  }

  $draggable.removeClass('ui-draggable-dragging');
  $draggable.appendTo($dropArea);

  ref.updateCheck2();

  // Limit to only 10 items.
  if ($dropArea[0].childElementCount >= 10) {
    $dropArea.droppable("disable");
  }

  var removeDraggable = function() {
    $draggable.draggable("destroy");
    $draggable.remove();
    ref.updateCheck2();

    // Limit to only 10 items.
    if ($dropArea[0].childElementCount < 10) {
      $dropArea.droppable("enable");
    }
  };

  // Make this object draggable.
  $draggable.draggable({
    revert: "false",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: "original",
    containment: '.activity-content-area',
    stop: function() {
      if (!$draggable.hasClass('placed')) {
        removeDraggable();
      }
    }
  });

  // Add accessibility handler.
  $draggable.off('keydown');
  $draggable.on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      removeDraggable();
    }
  });
};

/**
 * Step 4: Listen to the audio.
 */
ActivityModel.prototype.step4Handler = function() {
  this.removeValidationStyles(); // Remove all (in)correct styles
  this.disableDoneButton(true);

  // Add a handler to listen to the audio player.
  this.onStepAudioEnded = this.waitForAudio.bind(this);
};

/**
 * Step 5: End screen.
 */
ActivityModel.prototype.step5Handler = function() {
  // Nothing. Just listen to the audio.
  this.onStepAudioEnded = null;
};

/**
 * Reset button handler.
 */
ActivityModel.prototype.resetActivityHandler = function() {

  this.onStepAudioEnded = null;
  this.disableNextButton(false); // Enable the next button
  this.disableDoneButton(true); // Disable the done button
  this.removeValidationStyles(); // Remove all (in)correct styles

  for (var i = 1; i <= this.curSlide; i++) {
    var elem = ".activity-slide[data-slide='"+i+"'] ";

    if ($(elem+" .draggable-item").hasClass("ui-draggable")) {
      $(elem+" .draggable-item").draggable("destroy");
    }
    if ($(elem+" .drop-area"+i+" .drop-area-block"+i).hasClass("ui-droppable")) {
      $(elem+" .drop-area"+i+" .drop-area-block"+i).droppable("destroy");
    }
  }
  $(".activity .drop-area .drop-area-block .draggable-item").remove();

  // Clean-up events.
  if (this.audioHandler !== null) {
    var audioPlayer = $(".activity-audio[data-slide='4']")[0];
    audioPlayer.removeEventListener("ended", this.audioHandler);
  }
};

/**
 * Wait until the audio has finished then enable the audio.
 */
ActivityModel.prototype.waitForAudio = function() {
  this.disableDoneButton(false);
};
