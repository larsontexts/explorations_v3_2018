var Exploration = function () {

	var panels = [],
		language = "en",
		audio = new Audio(),
		audio_index = 0,
		sound = true,
		caption = true,
		step = 0,
		int_val = null,
		next_button = new Button('next'),
		back_button = new Button('back'),
		play_button = new Button('playback'),
		go_to_first = new Button('go-to-start');

		var useLocalFiles = false;

		//missing audio failover
		audio.onerror = function(e){	
			//console.log('failover audio catch', e);
			setTimeout(function(){
				if (stepContainsValidLearnosity()) {
					$("#next-button").removeClass('disabled');
				}
			}, 
			playback_timeout);
		}

	/**maintain functionality of auto-navigation in case of missing auido files */
	var playback_timeout = 500;


	useLocalAudioFiles = function(){
		useLocalFiles = true;
	}

	bindCheckAnswerButtonCreation = function(){
		var lrnApp = null;
		if (lrnApp = learnosity()) {
			var questions = lrnApp.questions();
			var step = steps[get_current_step() - 1];
			var $container = $("#" + step.id);
			var $activities = $container.find('.learnosity-item');
			var sessionID = "";
			var items = [];

			$activities.each(function () {
				//get item id 
				items.push(this.querySelector('[id^="' + this.dataset.sessionId + '"]').id);
				sessionID = this.dataset.sessionId;
			});


			if(questions){
				for(let i in questions){
					var question = questions[i];					
					if(question.getScore() !== null){
						
						lrnApp.questions()[i].id = i;
						//only offer check answer if question has validation
						if(lrnApp.questions()[i].validate){
							lrnApp.questions()[i].on('change', function(){							
								append_check_answer_button(this);
							});
						}
					}
				}
			}
			return false;
		}
	}

	stepContainsValidLearnosity = function () {
		var lrnApp = null;
		if (lrnApp = learnosity()) {
			var questions = lrnApp.questions();
			var step = steps[get_current_step() - 1];
			var $container = $("#" + step.id);
			var $item = $container.find('.learnosity-item');
			var question_ids = [];
			
			
			valid = true;
			$item.each(function () {

				var $questions_elements = $container.find('[id^="' + this.dataset.sessionId + '"]');
				//get question ids
				$questions_elements.each(function(){
					question_ids.push(this.id);

				});
			});

			if(!question_ids || question_ids.length == 0)
				return true;
			
			for (let i in questions) {
				//a question in this step
				if (question_ids.indexOf(i) !== -1) {

					//var isTextBox = questions[i].getQuestion().type == "plaintext" || questions[i].getQuestion().type == 'longtext' ;
					var isNonValidatingItem  = (questions[i].getScore() === null);
					// auto- validate any text entry questions
					var questionStatus = questions[i].isValid() || isNonValidatingItem;
					if(!questionStatus)
					valid = false;
				}
			}
		
		}
		return valid;
	}

	scorableQuestionIsComplete = function(){
		var lrnApp = null;
		if (lrnApp = learnosity()) {
			var questions = lrnApp.questions();
			var step = steps[get_current_step() - 1];
			var $container = $("#" + step.id);
			var $activities = $container.find('.learnosity-item');
			var items = [];
			var valid = false;

			$activities.each(function () {
				//get item id 
				items.push(this.querySelector('[id^="' + this.dataset.sessionId + '"]').id);
			});
			for (let i in questions) {
				//a question in this step
				if (items.indexOf(i) !== -1) {
					//non validating item
					if(questions[i].getScore() === null)
						return false;
						var scoreData = questions[i].getScore();
						return (scoreData.score == scoreData.max_score) ? i : false;
				}
			}
		}
	}

	lockQuestion = function(question_id){
		var question = document.getElementById(question_id);

		
		if(question.dataset.locked == true)
			return;
		question.dataset.locked = true;

		var q_coords = question.getBoundingClientRect();
		var splash = document.createElement('div');
		splash.classList.add('splash-guard');
		splash.style.position = "absolute";
		splash.style.width = q_coords.width + "px";
		splash.style.height = q_coords.height + "px";
		splash.style.top = q_coords.top + "px";
		splash.style.zIndex = "1001";
		splash.style.cursor = "not-allowed";
		splash.style.opacity = "0.3";
		question.parentElement.appendChild(splash);

		splash.onclick = function(evt){
			evt.preventDefault();
		}
	}

	learnosity = function () {
		return null || window.itmAp;
	}

	getCurrentLearnosityItem = function(){
		let current_step = steps[get_current_step() - 1];
		let targetElement = document.getElementById(current_step.id);
		let refId = $(targetElement).find('.learnosity-item').data('reference');

		if(learnosity()){
			var items = learnosity().getItems();
			for(let i in items){
				if(refId == i)
					return items[i];
			}
		}
		
	}

	/**call learnosity's default UI validation function - 
	 * puts X's and Checkmarks on quesiton input elements */
	learnosityValidation = function () {
		if (learnosity()) {
			var learnosityApp = itmAp;
			var current_step = get_current_step();
			var count = 1;
			var questions = learnosityApp.questions();

			//only need to evaluate if we're not on the last step
			if (current_step < stepcount){
				for (var i in questions){
					questions[i].validate();
				}
			}
		}
	}

	init = function (jsonData) {

		if(!jsonData)
			return;

		populate(jsonData);

		/*initialize the state of exploration for all controls */
		next_button.state(step);
		back_button.state(step);
		play_button.state(step);

		/*cleanup after audio playback end */
		audio.addEventListener("ended", function (i, e) {

			exploration.manageStepAudio();

			togglePlayButtonGlyph(play_button);

			if (stepContainsValidLearnosity()) {
				$("#next-button").removeClass('disabled');
			}
		});
	},

	/*interpolate data from object array in index.html */
	populate = function (jsonData) {

		for (var i = 0; i < jsonData.length; i++) {

			var audio_instance = jsonData[i];
			var panel_audio = audio_instance.audio;

			exploration.setPanel(audio_instance.id);

			/*load each audio file for this panel */
			for (var j = 0; j < panel_audio.length; j++) {
				var audio_item = panel_audio[j];
				setAudio(i, audio_item.file, audio_item.captionText);
			}
		}
	},

	/*exploration step may have more than one audio file/caption pair.  trigger each file/caption addition 
	at the end of the preceding file playback.  @end of array, or if only one */
	manageStepAudio = function () {

		var panel = get_panel(step - 1);
		var next_index = (audio_index + 1);

		/*is there another audio item for this panel? */
		if (next_index < panel.audio_length()) {
			/*update audio file source*/
			load_audio_source(next_index);
			/*append caption source */
			var new_audio_caption = panel.audio(next_index).caption;
			var $caption = $("#captionText");
			$caption.text($caption.text() + "  " + new_audio_caption);
			audio.play();
		}
		/*we've reached the end of the audio item array.  Reset to user can listen again if they wish, and give visual confirmation that all files have finished playing*/
		else {
			reset_audio();
			//trigger_flashy_button();
		}
	},

		reset_audio = function () {
			/*audio.currentTime = 0;

			//reset 'play'button
			$("#" + play_button.component_id('button')).data('value', 'pause');
			$("#" + play_button.component_id('icon')).removeClass('fa-pause');
			$("#" + play_button.component_id('icon')).addClass('fa-play');*/
		},

		trigger_flashy_button = function () {

			var int_count = 0;
			var $next_btn = $("#" + next_button.component_id('button'));

			if (($next_btn.hasClass('done')) || ($next_btn.hasClass('finished')))
				return false;

			int_val = setInterval(function () {
				$("#next-button").toggleClass('flashy');
				if (int_count > 4) {
					clearInterval(int_val);
					int_val = null;
					return;
				}
				int_count = int_count + 1;
			}, 500);
		},

		setPanel = function (panel_id) {

			var panel = new Panel(panel_id);
			panels.push(panel);
			///**return index of panel */
			return (panels.length - 1);
		},

		get_panel = function (index) {
			return panels[index];
		},

		get_current_step = function () {
			return step;
		},

		setAudio = function (index, file_name, text) { panels[index].add_audio(file_name, text)},

		set_language = function (new_lang) {
			language = new_lang;
		},

		_length = function () {
			return panels.length;
		},
		current_panel_id = function () {

			return panels[(step - 1)].id;
		},
		/*load first audio element for given panel */
		load_audio_source = function (selected_index) {

			if (selected_index === undefined)
				var selected_index = 0;

			audio_index = selected_index; //remember the index of audio array for this  exploration step

			var file_suffix = (language === 'es') ? "_sp" : "",
					file_prefix = useLocalFiles ? "./assets/audio/" : "", 
					current_index = step == 0 ? step : step - 1;
				audio_object = panels[current_index].audio(selected_index);
			filename = audio_object.file;
			audio.src = file_prefix + filename + file_suffix + ".mp3";
		},

		load_caption = function (index) {

			index = 0 || index;
			var current_index = step == 0 ? step : step - 1;
			var audio_object = panels[current_index].audio(index);
		
			var text = audio_object.caption;
			document.getElementById('captionText').innerText = text;
		},

		load_media = function () {
			load_audio_source();
			load_caption();
		},

		update_step = function (increment) {

			increment = null || increment;

			if (increment) {
				step = (step === panels.length) ? step : step + 1;

			} else {
				step = (step === 1) ? step : step - 1;
			}

			/*inform our controls of what state the player is in */
			next_button.state(step);
			back_button.state(step);
			play_button.state(step);

			/*take care of locking the back button */
			toggle_back_button_disable();
			/*manage lockdown of go-to controls */
			toggle_go_to_controls();

			bindCheckAnswerButtonCreation();

			
		},

		append_check_answer_button = function (question) {

			var target = document.getElementById(question.id);
			var items = learnosity().getItems();
			if(target){

				if(target.dataset){
					if(target.dataset.chkAppended)
						return false;
					else 
					target.dataset.chkAppended = true;
				}

				var btn = document.createElement('div');
				btn.id = "check-answers-button-" + question.id;
				btn.dataset.questionID = question.id;
				btn.classList.add('btn-secondary', 'chk-ans-btn');
				var chkImg = document.createElement('img');
				chkImg.id = "check-answers-button-icon-" + question.id;
				chkImg.src = "../bil-assets/images/k-5-check.svg";
				var chkTxt = document.createElement('span');
				chkTxt.id = "check-answers-button-text-" + question.id;
				chkTxt.innerText = "Check";
				btn.appendChild(chkImg);
				btn.appendChild(chkTxt);
				target.appendChild(btn);

				//adjust container height
				let curHeight = target.getBoundingClientRect().height;
				if (100 > curHeight + 100)
					target.style.height = (curHeight + 100) + "px";				
			}
		}, 

		appendShowMeButton = function(questionID){

			let parent = document.getElementById('check-answers-button-'+questionID).parentElement;

				if(parent.dataset){
					if(parent.dataset.showBtn == 'set')
						return false;
					parent.dataset.showBtn = 'set';
				}

			var showBtn = document.createElement('button');
			var showImg = document.createElement('img');
			var showText = document.createElement('span');
			
			showBtn.classList.add('btn-secondary', 'btn-show-me');
			showBtn.dataset.refId = questionID;
			showBtn.id = "show_btn";

			showImg.src = "../bil-assets/images/k-5-help.svg";

			showText.innerText = "Show Me";

			showBtn.appendChild(showImg);
			showBtn.appendChild(showText);

			parent.appendChild(showBtn);

		}


		appendTryAgainButton = function(questionID) {

			let parent = document.getElementById('check-answers-button-'+questionID).parentElement;

				if(parent.dataset){
					if(parent.dataset.tryBtn == 'set')
						return false;
					parent.dataset.tryBtn = 'set';
				}

				var tryBtn = document.createElement('button');
				var tryImg = document.createElement('img');
				var tryText = document.createElement('span');
			
				tryBtn.classList.add('btn-secondary', 'btn-try-again');
				tryBtn.dataset.refId = questionID;
	
				tryImg.src = "../bil-assets/images/k-5-try-again.png";

				tryBtn.id = "try_btn";
				tryText.innerText = "Try Again";
				
				tryBtn.appendChild(tryImg);
				tryBtn.appendChild(tryText);

				parent.appendChild(tryBtn);
		} 

		toggle_back_button_disable = function () {
			if (step == 1)
				$("#" + back_button.component_id('button')).addClass('disabled');
			else
				$("#" + back_button.component_id('button')).removeClass('disabled');
		},


		toggle_go_to_controls = function () {

			if (step == 1) {
				//$("#go-to-start-button").addClass('goto-disabled');
				return;
			}
			if (step == _length()) {
				//$("#go-to-end-button").addClass('goto-disabled');
				return;
			}
			$("#go-to-start-button").removeClass('goto-disabled');
			$("#go-to-end-button").removeClass('goto-disabled');
		};

	return {
		init: init,
		setPanel: setPanel,
		get_panel: get_panel,
		get_current_step: get_current_step,
		setAudio: setAudio,
		set_language: set_language,
		load_media: load_media,
		current_panel_id: current_panel_id,
		update_step: update_step,
		sound: sound,
		caption: caption,
		language: language,
		audio: audio,
		_length: _length,
		step: step,
		next_button: next_button,
		back_button: back_button,
		play_button: play_button,
		audio_index: audio_index,
		manageStepAudio: manageStepAudio,
		useLocalAudioFiles : useLocalAudioFiles, 
		stepContainsValidLearnosity:stepContainsValidLearnosity
	};
};


var Panel = function (panel_id) {

	var id = panel_id,
		audio_elements = [],
		audio_length = 0,

		add_audio = function (file_name, text) {

			var item = {};
			item.file = file_name;
			item.caption = text;
			audio_elements.push(item);
			audio_length = audio_elements.length;
		},
		audio_length = function () {
			return audio_elements.length;
		},
		audio = function (index) {

			if (index === undefined)
				index = 0;
			return audio_elements[index];
		};

	return {
		add_audio: add_audio,
		audio_length: audio_length,
		audio: audio,
		id: id
	};
};

var Button = function (_id) {

	var id = _id,
		_state = 0,

		state = function (cur_state) {
			cur_state = null || cur_state;

			if (cur_state)
				_state = cur_state;
			else
				return _state;
		},
		component_id = function (component) {
			return id + "-" + component;
		},
		bind = function (event, callback) {

			var object = this;

			$("#" + component_id('button')).on(event, function () {
				callback(object);
			});
		}
	return {
		component_id: component_id,
		state: state,
		bind: bind
	}
};


function clearLearnosityFeedbackFromScreen(questionID){
		var event = new Event('click', {'bubbles': false,'cancelable': true});

		$("#"+questionID).find('*').each(function(){this.dispatchEvent(event);});
}

/*EVENT BINDING*/
$('document').ready(function () {

	/**SHOW ME button click */

	$(document).on('click', '.btn-show-me', function(){
		if(learnosity()){
			let questionID = this.dataset.refId;;
			let questions = itmAp.questions();

			for(let i in questions){
				if (i == questionID){
					clearLearnosityFeedbackFromScreen(i)
					questions[i].validate({"showCorrectAnswers":true});
				}
			}
			//var qsKeys = Object.keys(itmAp.questions());
			//let itemIndex = qsKeys.indexOf(item.response_ids[0]);
			//itmAp.questions()[qsKeys[itemIndex]].validate({"showCorrectAnswers":true});
		}
	});

	$(document).on('click', '.btn-try-again', function(){
		clearLearnosityFeedbackFromScreen(this.dataset.refId);	
	});

	/**click handler for validation button */
	$(document).on("click", ".chk-ans-btn", function () {

		var questions = itmAp.questions();
		let questionID = this.dataset.questionID

		for(let i in questions){
			if(i == questionID){

				let question = questions[i];
				let scoreObj = question.getScore();
				question.validate();
				if(scoreObj){
					//check question scoring
					if(scoreObj.score == scoreObj.max_score){
						// unlock 'next' button if all answers in step are correct
						if(stepContainsValidLearnosity()){
							$("#next-button").removeClass('disabled');
							$('.btn-try-again').remove();
							$('.btn-show-me').remove();
							$("#check-answers-button-" +questionID).addClass('disabled');
							$("#check-answers-button-icon-" + questionID).remove();
						}
					} else {
						//append try it and showme buttons - wrong answer	
						appendTryAgainButton(i);
						appendShowMeButton(i);
					}
				}
			}
		}
	});
	/**adjust top and bottom padding  */
	setBodyPadding();

	/*bind control events to navigation buttons*/
	exploration.next_button.bind('click', nextButtonAction);
	exploration.play_button.bind('click', togglePlayButtonGlyph);
	exploration.play_button.bind('click', toggle_audio_playback);
	exploration.back_button.bind('click', back_button_action);

	/*LANGUAGE SELECTION*/
	$(".lang-option").on('click', function () {
		language_toggle($(this));
	});

	/*AUDIO TOGGLE*/
	$("#control-image-sound").on('click', function () {
		toggle_setting($(this), 'sound');
	});

	/*CAPTION TOGGLE*/
	$("#control-image-caption").on('click', function () {
		toggle_setting($(this), 'caption');
	});

	/*RESET TO FIRST STEP */
	$("#go-to-start-button").on('click', function () {

		if (saveState == true) {
			itmAp.save();
		}
		while (exploration.get_current_step() > 1) {
			$("#back-button").click();
		}
		exploration.reset_audio();
	});

	/*SKIP TO END STEP */
	$("#go-to-end-button").on('click', function () {

		if (saveState == true) {
			itmAp.save();
		}
		while (exploration.get_current_step() < exploration._length()) {
			$("#next-button").click();
		}
		exploration.reset_audio();
	});
});


/*FUNCTION DEFINITIONS*/
/*NEXT BUTTON CLICK*/
function nextButtonAction(btn_obj, isDev = false) {

	//take no action if button disabled

	if(!isDev){
		if ($("#next-button").hasClass('disabled')) {
			return false;
		}
	}
	
	//if is splash screen click, or is not hte final step, disable controls
	if ((exploration.get_current_step()  )< exploration._length()) {
		$("#next-button").addClass('disabled');
	
	}

	if (saveState == true) {
		itmAp.save();
	}

	if (exploration.get_current_step() <= exploration._length()) {


		var is_finished = $("#next-button").hasClass('done') || $("#next-button").hasClass('finished');

		exploration.update_step(increment = true);

		toggle_button_display(btn_obj, is_back = false);

		if (is_finished)
			return;

		var target_panelID =exploration.current_panel_id();
		target_panel = document.getElementById(target_panelID);
	
			$("#" + target_panelID).show();
			$("#" + target_panelID).css('visibility', 'visible');
		
		//document.getElementById(target_panel).style.opacity = "1";

		if(exploration.step !== 0){
			var curHeight = $("#" + target_panelID).height();
			var updated = curHeight + 100;
			$("#" + target_panelID).height(updated + "px");
		}
		
		scroll_to_element(target_panelID);

		var button_id = btn_obj.component_id('button');

		exploration.load_media();
		exploration.audio.play(); //trigger audio playback
		//update button state
		togglePlayButtonGlyph(exploration.play_button);
	}
}

function back_button_action(btn_obj) {

	if (saveState == true) {
		itmAp.save();
	}

	if (exploration.get_current_step() > 1) {

		var skip_decrement = $("#next-button").hasClass('finished');

		toggle_button_display(btn_obj, is_back = true);

		if (skip_decrement)
			return false;

		target_id = exploration.current_panel_id();
		$("#" + target_id).hide();

		exploration.update_step(increment = false);
		exploration.load_media();
		exploration.audio.play(); //trigger audio playback
	}
}

/**manage the display properties of next and back buttons*/
function toggle_button_display(btn_obj, is_back_button) {

	is_back_button = null || is_back_button;
	var num_steps = exploration._length();
	var cur_step = btn_obj.state();
	var $next_button = $('#next-button');
	var done = $next_button.hasClass('done');
	var finished = $next_button.hasClass('finished');

	if (cur_step === num_steps) {


		if (is_back_button) {
			if (finished) {
				toggle_finish_button_state(isfinished = false, 'done');
			}
			if (done) {
				toggle_finish_button_state(isfinished = false, 'none');
			}
		} else {
			if (done) {
				toggle_finish_button_state(isfinished = true);
			} else if ((!done) && (!finished))
				toggle_finish_button_state(isfinished = false, 'done');
		}

	} else if (cur_step === (num_steps - 1)) {

	}
}

function toggle_finish_button_state(is_finished, state) {

	is_finished = null || is_finished;
	state = "" || state;

	var $button = $("#next-button");
	var $span = $("#next-button-text");
	var $icon = $("#next-button-icon");

	if (is_finished) {

		$button.addClass('finished');
		$button.removeClass('done');
		$span.text("Done");
		//$icon.removeClass('fa-check');
		//$icon.addClass('fa-exclamation');

	} else {
		$button.removeClass('finished');
		$button.addClass((state === "done") ? "done" : "");
		$button.removeClass((state === "done") ? "" : "done");
		$span.text((state === "done") ? "Done" : "Next");
		$icon.removeClass('fa-exclamation');
		$icon.removeClass('fa-chevron-right');
		$icon.addClass((state === "done") ? "fa-check" : "fa-chevron-right");

	}
}

/*CAPTION / AUDIO UI TOGGLE - dis / able buttons */

function toggle_setting($caller, type) {
	var enabled = $caller.data('value') === "on" ? true : false;
	var new_value = (enabled) ? "off" : "on";
	exploration[type] = !enabled;
	$caller.data('value', new_value);

	if (type === 'caption') {
		toggle_caption_bar(new_value);
		return;
	}
	if (type === 'sound') {
		toggle_volume_level(new_value);
		$caller.attr('src', "../../images/sound-" + new_value + ".png");
	}
}

function toggle_volume_level(selector) {
	var level = (selector === 'on') ? 1 : 0;
	exploration.audio.volume = level;
}

/*CAPTION BAR TOGGLE - show/hide caption bar*/
function toggle_caption_bar(cur_val) {
	cur_val = null || cur_val;

	$("#captions").css('display', (cur_val === 'on') ? 'block' : 'none');
	setBodyPadding();
}

/*LANGUAGE SELECTION*/
function language_toggle($caller) {
	//buttons
	var $parent = $("#lang-opts");
	var expanded = $parent.hasClass("expanded");

	if (!expanded)
		$parent.addClass("expanded");

	else {
		/**same lang selected. do nothing */
		var self = $caller.hasClass('lang-selected');
		if (self) {
			$parent.removeClass("expanded");
			return;
		}
		/*determine language */
		let selected_id = $caller.attr('id');
		let inverse_id = (selected_id === "lang-select-en") ? "lang-select-es" : "lang-select-en";
		let selected_language = (selected_id === "lang-select-en") ? "en" : "es";

		/*set audio language*/
		exploration.set_language(selected_language);
		exploration.load_media();

		/*reset play button */
		$("#playPauseBtn").data('value', 'pause');
		$("#playback-icon").removeClass('fa-pause');
		$("#playback-icon").addClass('fa-play');


		/**toggle button look - selected class */
		$("#" + selected_id).addClass('lang-selected');
		$("#" + inverse_id).removeClass('lang-selected');

		/*collapse menu */
		if ($("#" + selected_id).index() === 0)
			$parent.removeClass('expanded');
		else {
			var $temp = $parent.children().eq(0).detach();
			$parent.append($temp);
			$parent.removeClass('expanded');
		}
	}
}

function togglePlayButtonGlyph(btn_obj) {

	var $parent = $("#" + btn_obj.component_id('button'));
	var $target = $("#" + btn_obj.component_id('icon'));
	var state = $parent.data('value');
	var new_state = (state === 'play') ? "pause" : 'play';

	$target.removeClass('fa-' + new_state);
	$target.addClass('fa-' + state);
	$parent.data('value', new_state);
}

function toggle_audio_playback(btn_obj) {

	var button_id = btn_obj.component_id('button');
	var $parent = $("#" + button_id);
	var state = $parent.data('value');

	if (state == 'play') {
		if (exploration.audio.ended) {
			exploration.audio.currentTime = 0;
		}
		exploration.audio.play();
	} else {
		exploration.audio.pause();
	}

	exploration.audio[state](); //call to play() || pause()
}

function setBodyPadding() {
	var viewArea = $(window).outerHeight() - ($('footer').outerHeight() + $('#stem').outerHeight());
	$('body').css('padding-bottom', $('footer').outerHeight());
	$('body').css('padding-top', $('#stem').outerHeight());

}

function scroll_to_element(stepId) {
	$("html, body").animate({
		scrollTop: $("#" + stepId).offset().top
	}, 800);
}

function devNext(){
	nextButtonAction(exploration.next_button, true);
}