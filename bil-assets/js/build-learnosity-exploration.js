var exploration_html = "<div class='row'>";
for (var i = 0; i < steps.length; i++) {
	
	var is_first = (0 === i);
	if (steps[i]['class'] === 'leftCol') {
		
		// break .row <div/> every .leftCol beyond the first
		if (!is_first) exploration_html += "</div><div class='row'>";
		
		// .leftCol
		exploration_html += "<div class='col-md-6 col-sm-7 leftCol'><div id='";
		exploration_html += steps[i]['id']+"'"+(is_first ? "" : " style='display:none;'")+">";
		for (var j = 0; j < steps[i]['learnosity'].length; j++)
			exploration_html += "<span class='learnosity-item' data-reference='"+steps[i]['learnosity'][j]+"'></span>";
		exploration_html += "</div></div>";

	} else {

		// .workArea
		exploration_html += "<div class='col-md-6 col-sm-5'><div class='workArea'><div style='display:none;' class='spaced screen' id='";
		exploration_html += steps[i]['id']+"'>";
		for (var j = 0; j < steps[i]['learnosity'].length; j++)
			exploration_html += "<span class='learnosity-item' data-reference='"+steps[i]['learnosity'][j]+"'></span>";
		exploration_html += "</div></div></div>";
	}
}
exploration_html += "</div>";

// echo
document.writeln(exploration_html);