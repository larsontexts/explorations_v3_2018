var itmAp;
var saveState = false;
var callSave;

execute = function() {

    var parameters = {};
    //found at: http://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-get-parameters
    //REQUIRES POLYFILL, found below
    var initOptions;
    var url = new URL(location.href);
    var searchParams = new URLSearchParams(url.search);
    var itemsCSVString = '';
    $.each($('.learnosity-item'), function() {
        itemsCSVString += $(this).data("reference") + ",";
    });
    itemsCSVString = itemsCSVString.substring(0, itemsCSVString.length - 1);
    parameters['items'] = itemsCSVString;
    parameters['activityId'] = searchParams.get('activityId');
    parameters['sessionId'] = searchParams.get('sessionId');
    parameters['itemPoolId'] = searchParams.get('itemPoolId');
    parameters['userId'] = searchParams.get('userId');
    parameters['state'] = searchParams.get('state');
    parameters['type'] = searchParams.get('type');
    parameters['itemsAPI'] = searchParams.get('itemsAPI');
    parameters['questionsAPI'] = searchParams.get('questionsAPI');
    parameters['domain'] = window.location.hostname;

    var queryString = '?' + (parameters['items'] == null ? '' : '&items=' + parameters['items']) +
        (parameters['activityId'] == null ? '' : '&activityReportingId=' + parameters['activityId']) +
        (parameters['sessionId'] == null ? '' : '&sessionId=' + parameters['sessionId']) +
        (parameters['itemPoolId'] == null ? '' : '&itemPoolId=' + parameters['itemPoolId']) +
        (parameters['userId'] == null ? '' : '&user_id=' + parameters['userId']) +
        (parameters['state'] == null ? '' : '&state=' + parameters['state']) +
        (parameters['type'] == null ? '' : '&type=' + parameters['type']) +
        (parameters['itemsAPI'] == null ? '' : '&itemsAPI=' + parameters['itemsAPI']) +
        (parameters['questionsAPI'] == null ? '' : '&questionsAPI=' + parameters['questionsAPI']) +
        (parameters['domain'] == null ? '' : '&domain=' + parameters['domain']);

    var itemsJs = "//items.learnosity.com";
    if (parameters['itemsAPI'] != null && parameters['itemsAPI'] != 'undefined') {
        itemsJs += '/?' + parameters['itemsAPI'];
    }

    var learnJs = document.createElement('script');
    learnJs.src = itemsJs;
    document.head.appendChild(learnJs);
    learnJs.onload = function() {

        $('document').ready(function() {

            if (itemsCSVString != '') { //don't call learnosity service with empty items string
                $.getJSON("https://static.bigideasmath.com/lgen/lgen3JK.php" + queryString)
                    .done(function(data) {
                        initOptions = data;
                        load_assessment();
                    }).fail(function(xhr) {
                        debugger;
                        console.log(xhr);
                    });

                function load_assessment() {

                    var eventOptions = {
                        readyListener: function(e) {															
							///////////////////////////////LearnAudio//////////////////////////////////////////////
							//var script = document.createElement('script');
							//document.head.appendChild(script);
							//script.type = 'text/javascript';
							/*pasted this script below*/
							//script.src = "http://www-staging.bigideasmath.com/assessmentSandbox/cdn-player/js/lrn-cdn-audio.js";
							//script.onload = function(){
								var parser = new LearnosityAudioParser();
								var is_mathy = (document.getElementsByClassName("lrn_math_static").length > 0);
								var waiting = null;

								/*ALERT CLIENT THAT EXPLORATION IS STARTABLE*/
								window.explorationInitialize();
							
								//**check for rendered mathquill before parsing audio text *
								if (is_mathy) {
									waiting = setInterval(function() {
										var rendered = true;
										var collection = document.getElementsByClassName("lrn_math_static");
							
										for (var i = 0; i < collection.length; i++) {
											if (!(collection[i].classList.contains("lrn_math_renderer_processed")))
												rendered = false;
										}
										if (rendered) {
											clearInterval(waiting);
										//	parser.render_playback_links();
										}
									}, 50);
								} 
								//else
								//parser.render_playback_links();
							//}
							///////////////////////////////End LearnAudio//////////////////////////////////////////////
						
						
                            if (initOptions['request']['type'] == 'submit_practice') {
                                saveState = true;
								var saveSettings = {
                                        success: function(response_ids) {
                                            console.log("save has been successful", response_ids);
                                        },
                                        error: function(e) {
                                            console.log("save has failed", e);
                                        },
                                        progress: function(e) {
                                            console.log("progress", e);
                                        }
                                };
                                $('.saveBtn').show();
                                $('.saveBtn').click(function() {                                    
                                    itemsApp.save(saveSettings);
                                });
								callSave = function(){
									console.log(getFormattedDate());
									//itmAp.save(saveSettings);
								}
								//itmAp.questions().each().on('change', callSave());
								$.each(itmAp.questions(), function (i,v){v.on('changed', debounce(callSave,1500, false))});
                            }
                        },
                        errorListener: function(e) {
                            // Adds a listener to all error
                            console.log(e);
                        }
                    };
                    var itemsApp = LearnosityItems.init(initOptions, eventOptions);
                    itmAp = itemsApp;
                }
            }
        });
    };
}

function getFormattedDate() {
    var date = new Date();
    var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    return str;
}


if (window.jQuery) execute();
else {
    var script = document.createElement('script');
    document.head.appendChild(script);
    script.type = 'text/javascript';
    script.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";

    script.onload = execute;
}


/*function debounce(func, wait, immediate) {
    // 'private' variable for instance
    // The returned function will be able to reference this due to closure.
    // Each call to the returned function will share this common timer.
    var timeout;           

    // Calling debounce returns a new anonymous function
    return function() {
        // reference the context and args for the setTimeout function
        var context = this, 
            args = arguments;

        // Should the function be called now? If immediate is true
        //   and not already in a timeout then the answer is: Yes
        var callNow = immediate && !timeout;

        // This is the basic debounce behaviour where you can call this 
        //   function several times, but it will only execute once 
        //   [before or after imposing a delay]. 
        //   Each time the returned function is called, the timer starts over.
        clearTimeout(timeout);   

        // Set the new timeout
        timeout = setTimeout(function() {

             // Inside the timeout function, clear the timeout variable
             // which will let the next execution run when in 'immediate' mode
             timeout = null;

             // Check if the function already ran with the immediate flag
             if (!immediate) {
               // Call the original function with apply
               // apply lets you define the 'this' object as well as the arguments 
               //    (both captured before setTimeout)
               func.apply(context, args);
             }
        }, wait);

        // Immediate mode and no wait timer? Execute the function..
        if (callNow) func.apply(context, args);  
     }; 
};
*/
function debounce(fn, wait) {
    var timeout=null;
    var c=function(){ clearTimeout(timeout); timeout=null; };
    var t=function(fn){ timeout=setTimeout(fn,wait); };
    return function() {
        var context=this;
        var args=arguments;
        var f=function(){ fn.apply(context,args); };
        if (!timeout) {
            t(c);
            f();
        } else {
            c();
            t(f);
        }
    }
}

// URL Polyfill
// Draft specification: https://url.spec.whatwg.org

// Notes:
// - Primarily useful for parsing URLs and modifying query parameters
// - Should work in IE8+ and everything more modern, with es5.js polyfills

(function(global) {
    'use strict';

    function isSequence(o) {
        if (!o) return false;
        if ('Symbol' in global && 'iterator' in global.Symbol &&
            typeof o[Symbol.iterator] === 'function') return true;
        if (Array.isArray(o)) return true;
        return false;
    }

    function toArray(iter) {
        return ('from' in Array) ? Array.from(iter) : Array.prototype.slice.call(iter);
    }

    (function() {

        // Browsers may have:
        // * No global URL object
        // * URL with static methods only - may have a dummy constructor
        // * URL with members except searchParams
        // * Full URL API support
        var origURL = global.URL;
        var nativeURL;
        try {
            if (origURL) {
                nativeURL = new global.URL('http://example.com');
                if ('searchParams' in nativeURL)
                    return;
                if (!('href' in nativeURL))
                    nativeURL = undefined;
            }
        } catch (_) {}

        // NOTE: Doesn't do the encoding/decoding dance
        function urlencoded_serialize(pairs) {
            var output = '',
                first = true;
            pairs.forEach(function(pair) {
                var name = encodeURIComponent(pair.name);
                var value = encodeURIComponent(pair.value);
                if (!first) output += '&';
                output += name + '=' + value;
                first = false;
            });
            return output.replace(/%20/g, '+');
        }

        // NOTE: Doesn't do the encoding/decoding dance
        function urlencoded_parse(input, isindex) {
            var sequences = input.split('&');
            if (isindex && sequences[0].indexOf('=') === -1)
                sequences[0] = '=' + sequences[0];
            var pairs = [];
            sequences.forEach(function(bytes) {
                if (bytes.length === 0) return;
                var index = bytes.indexOf('=');
                if (index !== -1) {
                    var name = bytes.substring(0, index);
                    var value = bytes.substring(index + 1);
                } else {
                    name = bytes;
                    value = '';
                }
                name = name.replace(/\+/g, ' ');
                value = value.replace(/\+/g, ' ');
                pairs.push({ name: name, value: value });
            });
            var output = [];
            pairs.forEach(function(pair) {
                output.push({
                    name: decodeURIComponent(pair.name),
                    value: decodeURIComponent(pair.value)
                });
            });
            return output;
        }

        function URLUtils(url) {
            if (nativeURL)
                return new origURL(url);
            var anchor = document.createElement('a');
            anchor.href = url;
            return anchor;
        }

        function URLSearchParams(init) {
            var $this = this;
            this._list = [];

            if (init === undefined || init === null) {
                // no-op
            } else if (init instanceof URLSearchParams) {
                // In ES6 init would be a sequence, but special case for ES5.
                this._list = urlencoded_parse(String(init));
            } else if (typeof init === 'object' && isSequence(init)) {
                toArray(init).forEach(function(e) {
                    if (!isSequence(e)) throw TypeError();
                    var nv = toArray(e);
                    if (nv.length !== 2) throw TypeError();
                    $this._list.push({ name: String(nv[0]), value: String(nv[1]) });
                });
            } else if (typeof init === 'object' && init) {
                Object.keys(init).forEach(function(key) {
                    $this._list.push({ name: String(key), value: String(init[key]) });
                });
            } else {
                init = String(init);
                if (init.substring(0, 1) === '?')
                    init = init.substring(1);
                this._list = urlencoded_parse(init);
            }

            this._url_object = null;
            this._setList = function(list) { if (!updating) $this._list = list; };

            var updating = false;
            this._update_steps = function() {
                if (updating) return;
                updating = true;

                if (!$this._url_object) return;

                // Partial workaround for IE issue with 'about:'
                if ($this._url_object.protocol === 'about:' &&
                    $this._url_object.pathname.indexOf('?') !== -1) {
                    $this._url_object.pathname = $this._url_object.pathname.split('?')[0];
                }

                $this._url_object.search = urlencoded_serialize($this._list);

                updating = false;
            };
        }


        Object.defineProperties(URLSearchParams.prototype, {
            append: {
                value: function(name, value) {
                    this._list.push({ name: name, value: value });
                    this._update_steps();
                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            'delete': {
                value: function(name) {
                    for (var i = 0; i < this._list.length;) {
                        if (this._list[i].name === name)
                            this._list.splice(i, 1);
                        else
                            ++i;
                    }
                    this._update_steps();
                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            get: {
                value: function(name) {
                    for (var i = 0; i < this._list.length; ++i) {
                        if (this._list[i].name === name)
                            return this._list[i].value;
                    }
                    return null;
                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            getAll: {
                value: function(name) {
                    var result = [];
                    for (var i = 0; i < this._list.length; ++i) {
                        if (this._list[i].name === name)
                            result.push(this._list[i].value);
                    }
                    return result;
                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            has: {
                value: function(name) {
                    for (var i = 0; i < this._list.length; ++i) {
                        if (this._list[i].name === name)
                            return true;
                    }
                    return false;
                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            set: {
                value: function(name, value) {
                    var found = false;
                    for (var i = 0; i < this._list.length;) {
                        if (this._list[i].name === name) {
                            if (!found) {
                                this._list[i].value = value;
                                found = true;
                                ++i;
                            } else {
                                this._list.splice(i, 1);
                            }
                        } else {
                            ++i;
                        }
                    }

                    if (!found)
                        this._list.push({ name: name, value: value });

                    this._update_steps();
                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            entries: {
                value: function() { return new Iterator(this._list, 'key+value'); },
                writable: true,
                enumerable: true,
                configurable: true
            },

            keys: {
                value: function() { return new Iterator(this._list, 'key'); },
                writable: true,
                enumerable: true,
                configurable: true
            },

            values: {
                value: function() { return new Iterator(this._list, 'value'); },
                writable: true,
                enumerable: true,
                configurable: true
            },

            forEach: {
                value: function(callback) {
                    var thisArg = (arguments.length > 1) ? arguments[1] : undefined;
                    this._list.forEach(function(pair, index) {
                        callback.call(thisArg, pair.value, pair.name);
                    });

                },
                writable: true,
                enumerable: true,
                configurable: true
            },

            toString: {
                value: function() {
                    return urlencoded_serialize(this._list);
                },
                writable: true,
                enumerable: false,
                configurable: true
            }
        });

        function Iterator(source, kind) {
            var index = 0;
            this['next'] = function() {
                if (index >= source.length)
                    return { done: true, value: undefined };
                var pair = source[index++];
                return {
                    done: false,
                    value: kind === 'key' ? pair.name : kind === 'value' ? pair.value : [pair.name, pair.value]
                };
            };
        }

        if ('Symbol' in global && 'iterator' in global.Symbol) {
            Object.defineProperty(URLSearchParams.prototype, global.Symbol.iterator, {
                value: URLSearchParams.prototype.entries,
                writable: true,
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Iterator.prototype, global.Symbol.iterator, {
                value: function() { return this; },
                writable: true,
                enumerable: true,
                configurable: true
            });
        }

        function URL(url, base) {
            if (!(this instanceof global.URL))
                throw new TypeError("Failed to construct 'URL': Please use the 'new' operator.");

            if (base) {
                url = (function() {
                    if (nativeURL) return new origURL(url, base).href;

                    var doc;
                    // Use another document/base tag/anchor for relative URL resolution, if possible
                    if (document.implementation && document.implementation.createHTMLDocument) {
                        doc = document.implementation.createHTMLDocument('');
                    } else if (document.implementation && document.implementation.createDocument) {
                        doc = document.implementation.createDocument('http://www.w3.org/1999/xhtml', 'html', null);
                        doc.documentElement.appendChild(doc.createElement('head'));
                        doc.documentElement.appendChild(doc.createElement('body'));
                    } else if (window.ActiveXObject) {
                        doc = new window.ActiveXObject('htmlfile');
                        doc.write('<head><\/head><body><\/body>');
                        doc.close();
                    }

                    if (!doc) throw Error('base not supported');

                    var baseTag = doc.createElement('base');
                    baseTag.href = base;
                    doc.getElementsByTagName('head')[0].appendChild(baseTag);
                    var anchor = doc.createElement('a');
                    anchor.href = url;
                    return anchor.href;
                }());
            }

            // An inner object implementing URLUtils (either a native URL
            // object or an HTMLAnchorElement instance) is used to perform the
            // URL algorithms. With full ES5 getter/setter support, return a
            // regular object For IE8's limited getter/setter support, a
            // different HTMLAnchorElement is returned with properties
            // overridden

            var instance = URLUtils(url || '');

            // Detect for ES5 getter/setter support
            // (an Object.defineProperties polyfill that doesn't support getters/setters may throw)
            var ES5_GET_SET = (function() {
                if (!('defineProperties' in Object)) return false;
                try {
                    var obj = {};
                    Object.defineProperties(obj, { prop: { 'get': function() { return true; } } });
                    return obj.prop;
                } catch (_) {
                    return false;
                }
            })();

            var self = ES5_GET_SET ? this : document.createElement('a');



            var query_object = new URLSearchParams(
                instance.search ? instance.search.substring(1) : null);
            query_object._url_object = self;

            Object.defineProperties(self, {
                href: {
                    get: function() { return instance.href; },
                    set: function(v) {
                        instance.href = v;
                        tidy_instance();
                        update_steps();
                    },
                    enumerable: true,
                    configurable: true
                },
                origin: {
                    get: function() {
                        if ('origin' in instance) return instance.origin;
                        return this.protocol + '//' + this.host;
                    },
                    enumerable: true,
                    configurable: true
                },
                protocol: {
                    get: function() { return instance.protocol; },
                    set: function(v) { instance.protocol = v; },
                    enumerable: true,
                    configurable: true
                },
                username: {
                    get: function() { return instance.username; },
                    set: function(v) { instance.username = v; },
                    enumerable: true,
                    configurable: true
                },
                password: {
                    get: function() { return instance.password; },
                    set: function(v) { instance.password = v; },
                    enumerable: true,
                    configurable: true
                },
                host: {
                    get: function() {
                        // IE returns default port in |host|
                        var re = { 'http:': /:80$/, 'https:': /:443$/, 'ftp:': /:21$/ }[instance.protocol];
                        return re ? instance.host.replace(re, '') : instance.host;
                    },
                    set: function(v) { instance.host = v; },
                    enumerable: true,
                    configurable: true
                },
                hostname: {
                    get: function() { return instance.hostname; },
                    set: function(v) { instance.hostname = v; },
                    enumerable: true,
                    configurable: true
                },
                port: {
                    get: function() { return instance.port; },
                    set: function(v) { instance.port = v; },
                    enumerable: true,
                    configurable: true
                },
                pathname: {
                    get: function() {
                        // IE does not include leading '/' in |pathname|
                        if (instance.pathname.charAt(0) !== '/') return '/' + instance.pathname;
                        return instance.pathname;
                    },
                    set: function(v) { instance.pathname = v; },
                    enumerable: true,
                    configurable: true
                },
                search: {
                    get: function() { return instance.search; },
                    set: function(v) {
                        if (instance.search === v) return;
                        instance.search = v;
                        tidy_instance();
                        update_steps();
                    },
                    enumerable: true,
                    configurable: true
                },
                searchParams: {
                    get: function() { return query_object; },
                    enumerable: true,
                    configurable: true
                },
                hash: {
                    get: function() { return instance.hash; },
                    set: function(v) {
                        instance.hash = v;
                        tidy_instance();
                    },
                    enumerable: true,
                    configurable: true
                },
                toString: {
                    value: function() { return instance.toString(); },
                    enumerable: false,
                    configurable: true
                },
                valueOf: {
                    value: function() { return instance.valueOf(); },
                    enumerable: false,
                    configurable: true
                }
            });

            function tidy_instance() {
                var href = instance.href.replace(/#$|\?$|\?(?=#)/g, '');
                if (instance.href !== href)
                    instance.href = href;
            }

            function update_steps() {
                query_object._setList(instance.search ? urlencoded_parse(instance.search.substring(1)) : []);
                query_object._update_steps();
            };

            return self;
        }

        if (origURL) {
            for (var i in origURL) {
                if (origURL.hasOwnProperty(i) && typeof origURL[i] === 'function')
                    URL[i] = origURL[i];
            }
        }

        global.URL = URL;
        global.URLSearchParams = URLSearchParams;
    }());

    // Patch native URLSearchParams constructor to handle sequences/records
    // if necessary.
    (function() {
        if (new global.URLSearchParams([
                ['a', 1]
            ]).get('a') === '1' &&
            new global.URLSearchParams({ a: 1 }).get('a') === '1')
            return;
        var orig = global.URLSearchParams;
        global.URLSearchParams = function(init) {
            if (init && typeof init === 'object' && isSequence(init)) {
                var o = new orig();
                toArray(init).forEach(function(e) {
                    if (!isSequence(e)) throw TypeError();
                    var nv = toArray(e);
                    if (nv.length !== 2) throw TypeError();
                    o.append(nv[0], nv[1]);
                });
                return o;
            } else if (init && typeof init === 'object') {
                o = new orig();
                Object.keys(init).forEach(function(key) {
                    o.set(key, init[key]);
                });
                return o;
            } else {
                return new orig(init);
            }
        };
    }());

}(self));


///*Learnosity Audio*
/*
//**ready event listener*

readyListener: function(e) {

    var parser = new LearnosityAudioParser();
    var is_mathy = (document.getElementsByClassName("lrn_math_static").length > 0);
    var waiting = null;

    //**check for rendered mathquill before parsing audio text *
    if (is_mathy) {
        waiting = setInterval(function() {
            var rendered = true;
            var collection = document.getElementsByClassName("lrn_math_static");

            for (var i = 0; i < collection.length; i++) {
                if (!(collection[i].classList.contains("lrn_math_renderer_processed")))
                    rendered = false;
            }
            if (rendered) {
                clearInterval(waiting);
                parser.render_playback_links();
            }
        }, 50);
    } else
        parser.render_playback_links();
}
*/

var LearnosityAudioParser = (function () {
    function LearnosityAudioParser() {
        this.config = {
            targets: ['lrn_stimulus_content', 'lrn_sharedpassage', 'lrn_item_processed', 'lrn_response'],
            input_flags: ['input', 'dropdown', 'lrn_dragdrop ', 'lrn_dropzone', 'lrn_dragdrop', 'lrn_response_input', 'lrn_textinput', 'lrn_mcqgroup', 'lrn_editor_area', 'lrn_canvas_drawing'],
            audio_http_request: null
        };
        this.appendControls(); ///**add audio element controls */
    }
    ///**append playback links to text */
    LearnosityAudioParser.prototype.render_playback_links = function () {
        for (var i = 0; i < this.config.targets.length; i++) {
            var collection = document.getElementsByClassName(this.config.targets[i]);
            var is_response = (this.config.targets[i] === "lrn_response");
            for (var j = 0; j < collection.length; j++) {
                var elem_classes = collection[j].classList;
                var is_processed = (collection[j].dataset) ? ((collection[j].dataset.processed) ? collection[j].dataset.processed : false) : false;
                ///**only process each element once */
                if (is_processed)
                    return;
                collection[j].setAttribute('data-processed', 'true');
                /* get nodes parent reference id */
                var id_found = false;
                var reference_id = "";
                var parent_1 = collection[j].parentElement;
                while ((parent_1) && (!id_found)) {
                    if (parent_1.dataset.reference) {
                        reference_id = parent_1.dataset.reference;
                        id_found = true;
                    }
                    parent_1 = parent_1.parentElement;
                }
                var input_parsed = false;
                ///**don't parse text from inputs, but look for valid questions embedded in response wrapper*/
                if (is_response) {
                    var input_children = collection[j].getElementsByTagName("p");
                    for (var m = 0; m < input_children.length; m++) {
                        var child = input_children[m];
                        var grandchildren = child.children;
                        if (grandchildren.length == 0) {
                            /*no input elements detected, check for text */
                            this.insert_audio_playback(child, reference_id, "lsp" + j + "-" + m);
                        }
                        else {
                            var is_input = false;
                            for (var n = 0; n < grandchildren.length; n++) {
                                var grandchild = grandchildren[n];
                                for (var k in this.config.input_flags) {
                                    if (grandchild.classList.contains(this.config.input_flags[k]))
                                        is_input = true;
                                }
                            }
                            if (!is_input) {
                                this.insert_audio_playback(child, reference_id, "lsp" + j + "-" + m);
                            }
                        }
                    }
                    input_parsed = true;
                }
                if (input_parsed) {
                    continue;
                }
                ////*identify embedded images*/
                this.tag_embedded_images(collection[j]);
                ///**scrape text */
                this.insert_audio_playback(collection[j], reference_id, "lsp" + j);
            }
        }
    };
    ///**create  <audio> for playback*/
    LearnosityAudioParser.prototype.appendControls = function () {
        var audio_player = document.createElement('audio');
        audio_player.id = "audio_player";
        document.body.appendChild(audio_player);
    };
    ///**mark images that are embedded in speech-text strings */
    LearnosityAudioParser.prototype.tag_embedded_images = function (element) {
        var max_width = 50, max_height = 50, images = element.getElementsByTagName('img');
        if (images.length > 0) {
            for (var i = 0; i < images.length; i++) {
                var coords = images[i].getBoundingClientRect();
                if ((coords.width <= max_width) && (coords.height <= max_height))
                    images[i].classList.add('alt-embed');
            }
        }
    };
    ///**create speech-text string, append 'play button' to element */
    LearnosityAudioParser.prototype.insert_audio_playback = function (element, reference, id) {
        var target = element.cloneNode(true), text = this.get_speech_text(target);
        text = this.handle_errant_math_symbols(text); //replace HTML symbols (+,%,*) that may be flushed by server snitation with appropriate text string
        text = this.clean_whitespace(text);
        ///**no legit text, don't bother with button */
        if (text.length < 2)
            return;
        var hasher = new Md5_Hash(text);
        var md5 = hasher.MD5(), info = { "md5": md5, "text": text, "source": reference, "index": id }, button = document.createElement('img');
        button.src = "http://static.bigideasmath.com/other/cdn-audio-assets/sound-on.svg";
        button.title = "Listen to this text";
        button.alt = "Click to play nearby text";
        button.id = "playback_" + id;
        button.name = "play_btn_" + md5;
        button.dataset.md5 = md5;
        button.dataset.text = text;
        button.classList.add('audio-play');
        var obj = this;
        button.onclick = function (evt) {
            var btn_data = evt.target.dataset;
            obj.retrieve_audio(btn_data.md5, btn_data.text);
        };
        /**save info to container for future retrival */
        element.classList.add('item-container');
	//console.log(element.firstChild.nodeName);
		if(element.firstChild.nodeName == "P"){
			element.firstChild.prepend(button);
		}
        else {
			//console.log($(element).firstChild.prop('nodeName'));
			element.prepend(button);
		}
    };
    /**remove excess spacees */
    LearnosityAudioParser.prototype.clean_whitespace = function (input) {
        var parsed = input.replace(/\u00A0/g, ' '); //kill hardcoded unicode-encoded(&nbsp;) spaces
        parsed = parsed.replace(/\s/g, ' '); //html encoded (&nbsp;) spaces
        parsed = parsed.replace(/\s{2,}/g, ' ');
        return parsed.replace(/^\s+|\s+$/gm, ''); //trim leading and trailing spaces
    };
    LearnosityAudioParser.prototype.get_speech_text = function (element) {
        var text_string = "", maths = element.getElementsByClassName('lrn_math_renderer_processed'), images = element.getElementsByClassName('alt-embed'), tables = element.getElementsByTagName('table'), bad_math = false, //flag for hosed aria labels on mathquill
        is_input = false;
        for (var i = 0; i < this.config.input_flags.length; i++) {
            if (element.getElementsByClassName(this.config.input_flags[i]).length > 0)
                is_input = true;
        }
        if (is_input)
            return "";
        /*get element text */
        text_string = element.innerHTML;
        /*skip text from tables */
        if (tables.length > 0) {
            for (var t = 0; t < tables.length; t++) {
                var tbl_text = tables[t].innerHTML;
                text_string = text_string.replace(tbl_text, "");
            }
        }
        /*re-write math text*/
        if (maths.length > 0) {
            for (var m = 0; m < maths.length; m++) {
                var math_text = maths[m].innerHTML;
                var aria_text = maths[m].getElementsByClassName('lrn-display-offscreen')[0].innerText;
                text_string = text_string.replace(math_text, aria_text);
                if (aria_text == "")
                    bad_math = true;
            }
        }
        if (images.length > 0) {
            for (var p = 0; p < images.length; p++) {
                var img = images[p];
                var img_text = "";
                if (!((img.dataset.img_tts)) || (img.dataset.img_tts == "undefined"))
                    img_text = img.alt;
                else
                    img_text = img.dataset.img_tts;
                text_string = text_string.replace(img.outerHTML, " " + img_text + " ");
            }
        }
        var temp = document.createElement('span');
        temp.innerHTML = text_string;
        var clean_text = temp.innerText;
        return (clean_text.length > 1) ? clean_text : "";
    };
    LearnosityAudioParser.prototype.handle_errant_math_symbols = function (text) {
        var new_text = text.replace(/</g, ' less than ');
        new_text = new_text.replace(/>/g, ' greater than ');
        new_text = new_text.replace(/=/g, ' equals ');
        new_text = new_text.replace(/\+/g, ' plus ');
        return new_text;
    };
    LearnosityAudioParser.prototype.retrieve_audio = function (hash, text) {
        var base_url = "https://d17007d7ed144e385df3-0264cef6aad1b81efe617b95ad9e9064.ssl.cf2.rackcdn.com";
        var hash_dir = hash[0].toString() + hash[1].toString() + hash[2].toString();
        var tts_location = "/tts/e/";
        var md5 = hash;
        var tts_url = base_url + "/" + hash_dir + tts_location + hash + ".mp3";
        var obj = this;
        ///**check for existing audio file */
        var request = new XMLHttpRequest();
        request.onload = function (xhr) {
            var response_url = xhr.currentTarget.responseURL;
            var url = tts_url;
            obj.play_audio(response_url);
        };
        ///**tts recording does not exist, send request to server to generate audio */
        request.onerror = function (xhr) {
            var speech_text = text;
            var hash = md5;
            var srvr = "http://104.130.1.87/cdn-audio/index.php";
            var params = "?text=" + encodeURIComponent(speech_text) + "&hash=" + hash + "&action=learnosity_tts_audio_only";
            var destination = srvr + params;
            var req = new XMLHttpRequest();
            req.onload = function () {
                var str = request.responseText;
                var reply = req.responseText;
                var data = JSON.parse(reply);
                var path = data.filename;
                var base_url = "https://d17007d7ed144e385df3-0264cef6aad1b81efe617b95ad9e9064.ssl.cf2.rackcdn.com";
                obj.play_audio(base_url + "/" + path);
            };
            req.onerror = function (xhr) {
                console.log('fail to retrieve server response - ' + xhr);
            };
            req.open('POST', destination, true);
            req.send();
        };
        request.open('HEAD', tts_url, true);
        request.send();
    };
    LearnosityAudioParser.prototype.play_audio = function (audio_url) {
        var player = document.getElementById('audio_player');
        while (player.firstChild)
            player.removeChild(player.firstChild);
        var source = document.createElement('source');
        source.src = audio_url;
        player.appendChild(source);
        player.load();
        player.play();
    };
    return LearnosityAudioParser;
}());
///**hashing functions */
/**
 *
 *  MD5 (Message-Digest Algorithm)
 *  http://www.webtoolkit.info/
 *
 **/
var Md5_Hash = (function () {
    function Md5_Hash(input_text) {
        this._text = input_text;
    }
    ;
    Md5_Hash.prototype.MD5 = function () {
        var string = this._text;
        function RotateLeft(lValue, iShiftBits) {
            return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
        }
        function AddUnsigned(lX, lY) {
            var lX4, lY4, lX8, lY8, lResult;
            lX8 = (lX & 0x80000000);
            lY8 = (lY & 0x80000000);
            lX4 = (lX & 0x40000000);
            lY4 = (lY & 0x40000000);
            lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
            if (lX4 & lY4) {
                return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
            }
            if (lX4 | lY4) {
                if (lResult & 0x40000000) {
                    return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                }
                else {
                    return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
            }
            else {
                return (lResult ^ lX8 ^ lY8);
            }
        }
        function F(x, y, z) { return (x & y) | ((~x) & z); }
        function G(x, y, z) { return (x & z) | (y & (~z)); }
        function H(x, y, z) { return (x ^ y ^ z); }
        function I(x, y, z) { return (y ^ (x | (~z))); }
        function FF(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        ;
        function GG(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        ;
        function HH(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        ;
        function II(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        ;
        function ConvertToWordArray(string) {
            var lWordCount;
            var lMessageLength = string.length;
            var lNumberOfWords_temp1 = lMessageLength + 8;
            var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
            var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
            var lWordArray = Array(lNumberOfWords - 1);
            var lBytePosition = 0;
            var lByteCount = 0;
            while (lByteCount < lMessageLength) {
                lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                lBytePosition = (lByteCount % 4) * 8;
                lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
                lByteCount++;
            }
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
            lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
            lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
            return lWordArray;
        }
        ;
        function WordToHex(lValue) {
            var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
            for (lCount = 0; lCount <= 3; lCount++) {
                lByte = (lValue >>> (lCount * 8)) & 255;
                WordToHexValue_temp = "0" + lByte.toString(16);
                WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
            }
            return WordToHexValue;
        }
        ;
        function Utf8Encode(string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c_1 = string.charCodeAt(n);
                if (c_1 < 128) {
                    utftext += String.fromCharCode(c_1);
                }
                else if ((c_1 > 127) && (c_1 < 2048)) {
                    utftext += String.fromCharCode((c_1 >> 6) | 192);
                    utftext += String.fromCharCode((c_1 & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c_1 >> 12) | 224);
                    utftext += String.fromCharCode(((c_1 >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c_1 & 63) | 128);
                }
            }
            return utftext;
        }
        ;
        var x = Array();
        var k, AA, BB, CC, DD, a, b, c, d;
        var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
        var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
        var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
        var S41 = 6, S42 = 10, S43 = 15, S44 = 21;
        string = Utf8Encode(string);
        string = this.sanitize(string);
        x = ConvertToWordArray(string);
        a = 0x67452301;
        b = 0xEFCDAB89;
        c = 0x98BADCFE;
        d = 0x10325476;
        for (k = 0; k < x.length; k += 16) {
            AA = a;
            BB = b;
            CC = c;
            DD = d;
            a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
            d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
            c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
            b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
            a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
            d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
            c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
            b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
            a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
            d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
            c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
            b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
            a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
            d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
            c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
            b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
            a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
            d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
            c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
            b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
            a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
            d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
            c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
            b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
            a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
            d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
            c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
            b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
            a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
            d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
            c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
            b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
            a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
            d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
            c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
            b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
            a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
            d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
            c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
            b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
            a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
            d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
            c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
            b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
            a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
            d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
            c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
            b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
            a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
            d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
            c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
            b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
            a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
            d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
            c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
            b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
            a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
            d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
            c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
            b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
            a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
            d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
            c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
            b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
            a = AddUnsigned(a, AA);
            b = AddUnsigned(b, BB);
            c = AddUnsigned(c, CC);
            d = AddUnsigned(d, DD);
        }
        var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
        return temp.toLowerCase();
    };
    Md5_Hash.prototype.sanitize = function (string) {
        //trim whitespace
        var str = string.replace(/^\s+|\s+$/gm, '');
        //Using http://unicode-search.net/unicode-namesearch.pl
        //replace all unicode dash (minus) types with "hyphen-minus" '-'
        str = str.replace(/[\u00AD\u05BEâ€‹\u1806\u2010\u2011\u2012â€‹\u2013\u2014\u2015\u2212\uFE58\uFE63\uFF0D]/g, '\u002D');
        //replace Full-width Plus with Plus
        str = str.replace(/[\uFF0B\u2795]/g, '\u002b');
        //replace versions of multiplication signs with
        str = str.replace(/[\u2715\u2716]/g, '\u00D7');
        //replace double quotes with standard double quotes
        str = str.replace(/[\u201C\u201D\u201F\u275D\u275D\u275E\u301D\u301E\uFF02\u2033]/g, '\u0022');
        //replace double spaces with single spaces
        str = str.replace(/\s\s+/g, ' ');
        //replace curly apostrophes and prime with apostrophes
        str = str.replace(/[\u02BC\u055A\u07F4\u07F5\uFF07\u2018\u2019\u201B\u275B\u275C\u2032]/g, '\u0027');
        //replace elipses with ...
        str = str.replace(/[\u2026]/g, '...');
        //replace i acute with i
        str = str.replace(/[\u00CD\u00ED]/g, 'i');
        //replace Combining Left Arrow above with left arrow
        str = str.replace(/[\u20D6]/g, '\u2190');
        //replace Combining Right Arrow above with right arrow
        str = str.replace(/[\u20D7]/g, '\u2192');
        //replace left-pointing angle bracket with <
        str = str.replace(/[\u2329\u27E8\u3008]/g, '<');
        //replace right-pointing angle bracket with >
        str = str.replace(/[\u232A\u27E9\u3009]/g, '>');
        //WE MAY OR MAY NOT WANT TO DO THIS
        str = this.removePrivateUse(str);
        return str;
    };
    Md5_Hash.prototype.removePrivateUse = function (str) {
        //converts all private use unicode characters to '<|pu|>'
        //replace private use with <pu>
        var s = str.replace(/[\uE000-\uF8FF]/g, '<|pu|>');
        return s;
    };
    return Md5_Hash;
}());	