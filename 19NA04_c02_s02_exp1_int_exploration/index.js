var gradeChapter = "19NA04_c02";
var chapterSection = "s02";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{	
		'id':'screen1', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01', 
			'captionText':"Which addition problem shows a correct way to find thirty-eight plus seven? Select the correct problem."
		}]
	},
	{
		'id':'screen2', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02', 
			'captionText':"Write to explain why this is the correct way to show the problem."
		}]
	},
	{
		'id':'screen3', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03', 
			'captionText':"Which addition problem shows a correct way to find four hundred three plus one thousand two hundred forty-eight? "
		}]
	},
	{
		'id':'screen4', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'04', 
			'captionText':"Write to explain why this is the correct way to show the problem."
		}]
	},
	{
		'id':'screen5', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'05', 
			'captionText':"Why do you need to use place value when adding? Explain."
		}]
	}

];