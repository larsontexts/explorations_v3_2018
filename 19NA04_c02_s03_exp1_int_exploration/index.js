var gradeChapter = "19NA04_c02";
var chapterSection = "s03";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{	
		'id':'screen1', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01', 
			'captionText':"Which subtraction problem shows a correct way to find ninety-four minus eight? Select the correct problem"
		}]
	},
	{
		'id':'screen2', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02', 
			'captionText':"Write to explain why this is the correct way to show the problem"
		}]
	},
	{	
		'id':'screen3', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03', 
			'captionText':"Which subtraction problem shows a correct way to find three thousand seven hundred ten minus two hundred fifty-one? Select the correct problem."
		}]
	},
	{
		'id':'screen4', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'04', 
			'captionText':"Write to explain why this is the correct way to show the problem."
		}]
	},
	{
		'id':'screen5', 
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'05', 
			'captionText':"Why do you need to use place value when subtracting? Explain"
		}]
	}
];