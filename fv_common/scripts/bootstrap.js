/* global $ */
/* global URLSearchParams */
(function(namespace) {
  "use strict";
  namespace.bootstrap = function(ready) {
    $.when(
      $.getScript('../fv_plugins/jquery/jquery.loadTemplate.js'),
      $.getScript('../fv_common/scripts/shell.js'),
      $.getScript('../fv_common/scripts/locale.js'),
      $.Deferred(function(deferred) {
        $(deferred.resolve);
      })
    ).done(ready);
  };
  namespace.getLearnosity = function(wait, ready) {
    $.when(
      $.getScript('https://static.bigideasmath.com/lgen/learnosityHandler.js'),
      $.Deferred(function(deferred) {

        // Check for Spanish URL Params.
        var params = new URLSearchParams(new URL(location.href).search);
        if (params.get('lang') === "es") {
          // Change the active type to the Spanish option.
          $(".activity .activity-navigation .language-toggle .option").removeClass("active");
          $('#spanish-option').addClass('active');
        }

        function checkLoaded() {
          if (!wait) {
            $(deferred.resolve);
            return;
          }

          if (!namespace.itemsApp || !namespace.itemsApp.validItems || !namespace.itemsApp.validItems()){
            setTimeout(checkLoaded, 100);
          } else {
            $(deferred.resolve);
          }
        }
        checkLoaded();
      })
    ).done(ready);
  };
  namespace.bootstrapStandard = function(config, ready) {
    namespace.bootstrap(function() {
      config.$container.loadTemplate('../fv_common/template/shell.html', {}, {
        complete: function() {
          $('.audio-wrapper').loadTemplate(config.audio, {}, {
            complete: function() {
              $('.activity-wrapper').loadTemplate(config.template, {}, {
                complete: function() {
                  namespace.getLearnosity(config.waitForLearnosity, ready);
                }
              });
            }
          });
        }
      });
    });
  };
})(window);
