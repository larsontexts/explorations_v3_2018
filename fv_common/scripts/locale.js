/* global $ */
"use strict";

/**
 * Locale class.
 * Used to load locale files, and update objects with the proper locale strings.
 * @constructor
 */
function Locale() {
  this.translations = {
    en: {},
    es: {}
  };
  this.urls = [];
  this.loaded = 0;
  this.applyActivityLocale = function(lang) {};
}

/**
 * Constructor
 * @param {array} urls - an array of locale urls to load.
 */
Locale.prototype.init = function(urls) {
  if (urls === undefined) {
    urls = [];
  }

  // Add default translations.
  urls.push("../../fv_common/locale/common_en.json");
  this.urls = urls;

  // Append the translations by URL.
  console.log("Loading locale files:");
  this.loaded = 0;

  var ref = this;
  for (var index in urls) {
    if (index) {
      var url = urls[index];
      console.log("Fetching file: " + url);
      $.getJSON(url, function(json) {
        console.log("Parsing locale: " + url);
        ref.loadJson(json);
      })
        .fail(function(jqxhr, textStatus, error) {
          console.log("Error loading locale: " + url);
        });
    }
  }
};

/**
 * Loads a locale JSON file.
 * @param {json} json - the json for a language locale.
 */
Locale.prototype.loadJson = function(json) {
  var key;
  var value;

  // Load english translations.
  if (json.en !== undefined) {
    for (key in json.en) {
      if (key) {
        value = json.en[key];
        this.translations.en[key] = value;
      }
    }
  }

  // Load spanish translations.
  if (json.es !== undefined) {
    for (key in json.es) {
      if (key) {
        value = json.es[key];
        this.translations.es[key] = value;
      }
    }
  }

  this.loaded += 1;
  if (this.isLoaded()) {
    this.applyLocale("en");
  }
};

/**
 * Returns whether or not all of the locale json urls have been loaded.
 * @return {boolean} whether all locale files have been loaded.
 */
Locale.prototype.isLoaded = function() {
  return (this.urls.length > 0 && this.loaded === this.urls.length);
};

/**
 * Get the text for the given item in the supplied language.
 * @param {string} item - the key for the string.
 * @param {string} lang - the language for the string. Defaults to 'en'.
 * @return {string} the string for the key in the given language
 */
Locale.prototype.getParameter = function(item, lang) {
  if (lang === undefined) {
    lang = "en";
  }
  var value = this.translations[lang][item];
  if (value === undefined) {
    return String("[Unknown Value: " + item + " : " + lang + "]");
  }
  return value;
};

/**
 * Sets the activity specific locale settings.
 * @param {function} func - the function to apply for this activity.
 */
Locale.prototype.setLocaleFunc = function(func) {
  this.applyActivityLocale = func;
  if (this.isLoaded()) {
    this.applyLocale("en");
  }
};

/**
 * Applies the locale for all objects in the activity.
 * @param {string} lang - the languate id to apply.
 */
Locale.prototype.applyLocale = function(lang) {
  this.applyCommonLocale(lang);
  this.applyActivityLocale(lang);
};

/**
 * Applies locale to the common objects across all activities.
 * @param {string} lang - the language id to apply
 */
Locale.prototype.applyCommonLocale = function(lang) {

  // Navigation Buttons
  $('.btn-captions').attr('aria-label', this.getParameter('btn-captions-aria-label', lang));
  $('.btn-done').attr('aria-label', this.getParameter('btn-done-aria-label', lang));
  $('.btn-next').attr('aria-label', this.getParameter('btn-next-aria-label', lang));
  $('.btn-playpause').attr('aria-label', this.getParameter('btn-playpause-aria-label', lang));
  $('.btn-reset').attr('aria-label', this.getParameter('btn-reset-aria-label', lang));
  $('.btn-vol').attr('aria-label', this.getParameter('btn-vol-aria-label', lang));

  // Check buttons
  $('.btn-check').attr('aria-label', this.getParameter("btn-check-aria-label", lang));
  $('.btn-try-again').attr('aria-label', this.getParameter("btn-try-again-aria-label", lang));
  $('.btn-show-me').attr('aria-label', this.getParameter("btn-show-me-aria-label", lang));

  // Counters
  $('.counter-red').attr('aria-label', this.getParameter("counter-red-aria-label", lang));
  $('.counter-yellow').attr('aria-label', this.getParameter("counter-yellow-aria-label", lang));

  // Language Selection
  $('#english-option').attr('aria-label', this.getParameter("language-english-option-aria-label", lang));
  $('#spanish-option').attr('aria-label', this.getParameter("language-spanish-option-aria-label", lang));
};
