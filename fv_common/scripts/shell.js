/* global $ */
/* global itemsApp */
/* global URLSearchParams */
'use strict';

/**
 *
 * @constructor
 */
function ShellModel() {
  this.curSlide = 1;
  this.totalSlides = 1;
  this.selectedLang = "en";
  this.audioPlayer = null;
  this.locale = null;

  this.doneBtn = $(".btn-done");
  this.nextBtn = $(".btn-next");

  // Create the slide elements.
  this.slideElem = null;
  this.checkBtn = null;
  this.tryBtn = null;
  this.showBtn = null;

  this.params = new URLSearchParams(new URL(location.href).search);
}

ShellModel.prototype.initGlobal = function() {

  // Hide the loader.
  $('.loader').hide();

  // Show the Start button.
  $('.start-btn').show();

  // Check for Spanish URL Params.
  if (this.params.get('lang') === "es") {
    this.selectedLang = "es"; // Set the language type.

    // Change the active type to the Spanish option.
    $(".activity .activity-navigation .language-toggle .option").removeClass("active");
    $('#spanish-option').addClass('active');
  }

  // TODO: clean up this code
  var bool = false;
  var count = 0;
  var nex_cunt = 0;
  var flag_lag = false;
  $('a').first().trigger('focus');

  $(window).keyup(function(ee) {
    if (ee.target.className == 'option active') {
      flag_lag = true;
    }
  });
  $(".btn-next").on("keypress", function(e) {
    if (e.keyCode == 13 || e.keyCode == 32)
      nex_cunt++
    if (nex_cunt == 3) {
      setTimeout(function() {
        $(".btn-done").focus();
      }, 200)
    }
  });

  $('.btn-reset').on('keypress', function() {
    nex_cunt = 0;
  });
  $('.option').keydown(function(ee) {
    if ($(this).text() == 'Espanol' && ee.keyCode == 9 && !ee.shiftKey) {
      $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
      count = 0;
    } else if ($(this).text() == 'English' && ee.keyCode == 9 && ee.shiftKey) {
      $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
      count = 0;
    }
  });
  $('.language-toggle').keypress(function(eee) {
    if ((eee.keyCode == 13 && flag_lag == true) || (eee.keyCode == 32 && flag_lag == true)) {
      $(".activity .activity-navigation .language-toggle").removeClass("closed").addClass("open");
      $('.language-toggle span').each(function(eeee) {
        bool = $(this).hasClass("active");
        if ($(this)["0"].attributes["0"].value == eee.target.attributes["0"].value) {
          $('.option').removeClass('active');
          $(this).addClass('active');
          if ((bool == true) && (($(this).text() == "English") || ($(this).text() == "Espanol"))) {
            if (count == 0) {
              $(".activity .activity-navigation .language-toggle").removeClass("closed").addClass("open");
              count = 1;
            } else {
              $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
              count = 0;
            }

          } else {
            count = 0;
            $(".activity .activity-navigation .language-toggle").removeClass("open").addClass("closed");
          }
        }
      });
      flag_lag = false;
    }
  });
};

/**
 *
 * @param {number} totalSlides - the number of slides in the activity
 */
ShellModel.prototype.init = function(totalSlides) {
  var ref = this;
  this.totalSlides = totalSlides;

  $(".btn-audio-caption, .btn-reset, .btn-playpause, .btn-next, .btn-done").hide();

  $(".activity .activity-navigation .btn-vol").click(function() {
    ref.audioToggleHandler();
  });
  $(".activity .activity-navigation .btn-audio-caption").click(function() {
    ref.audioCaptionToggleHandler();
  });
  this.nextBtn.click(function() {
    ref.nextButtonHandler();
  });
  $(".activity .activity-navigation .btn-back").click(function() {
    ref.backButtonHandler();
  });
  $(".activity .activity-navigation .btn-reset").click(function() {
    ref.resetButtonHandler();
  });
  this.doneBtn.click(function() {
    ref.nextButtonHandler();
  });

  $(".activity .activity-navigation .btn-playpause").click(function() {
    if ($(this).hasClass("btn-play")) {
      ref.playAudioHandler();
    } else {
      ref.pauseAudioHandler();
    }
  });

  $(".activity .activity-navigation .language-toggle .option").click(function(event) {
    $(".activity .activity-navigation .language-toggle .option").removeClass("active");
    $(this).addClass("active");

    ref.selectedLang = $(this).attr("lang-id");

    var langSelector = $(".activity .activity-navigation .language-toggle");
    if (langSelector.hasClass("open")) {
      langSelector.removeClass("open").addClass("closed");

      // TESTING ONLY:
      if (ref.locale) {
        ref.locale.applyLocale(ref.selectedLang);
      }

    } else {
      langSelector.removeClass("closed").addClass("open");
    }
  });

  // Set the global aria-label button text.
  $('.btn-check').attr('aria-label', 'A check answer button with a check mark');
  $('.btn-try-again').attr('aria-label', 'A try again button with a backward arrow');
  $('.btn-show-me').attr('aria-label', 'A show answer button with a question mark');
};

/**
 *
 */
ShellModel.prototype.startActivity = function() {
  $(".activity .activity-wrapper").show();
  $(".btn-audio-caption,.btn-reset,.btn-playpause,.btn-next,.btn-done").show();
  this.showHideNextBackButtons();
  this.showSlide();
};

/**
 *
 */
ShellModel.prototype.audioToggleHandler = function() {
  var toggleVolBtn = $(".activity .activity-navigation .btn-vol");
  if (toggleVolBtn.hasClass("btn-audio-on")) {
    toggleVolBtn
      .removeClass("btn-audio-on")
      .addClass("btn-audio-off")
      .attr('aria-label', 'A mute button'); // TODO: use locale.
    this.muteUnmuteAudio(0);

  } else {
    toggleVolBtn
      .removeClass("btn-audio-off")
      .addClass("btn-audio-on")
      .attr('aria-label', 'An audio button'); // TODO: use locale.
    this.muteUnmuteAudio(1);
  }
};

/**
 *
 */
ShellModel.prototype.audioCaptionToggleHandler = function() {
  var caption = $(".activity  .activity-audio-caption");
  if (caption.is(":visible")) {
    caption.slideUp();

  } else {
    caption.slideDown();
  }
};

/**
 *
 */
ShellModel.prototype.nextButtonHandler = function() {
  if (this.curSlide < this.totalSlides) {
    this.curSlide = this.curSlide + 1;
    this.showHideNextBackButtons();
    this.showSlide();
  }
};

/**
 *
 */
ShellModel.prototype.backButtonHandler = function() {
  if (this.curSlide > 1) {
    this.curSlide = this.curSlide - 1;
    this.showHideNextBackButtons();
    this.showSlide();
  }
};

/**
 *
 */
ShellModel.prototype.resetButtonHandler = function() {
  this.curSlide = 1;
  this.resetActivityHandler();
  this.showHideNextBackButtons();
  this.showSlide();
};

/**
 *
 */
ShellModel.prototype.showHideNextBackButtons = function() {

  var backBtn = $(".activity .activity-navigation .btn-back");
  if (this.curSlide > 1) {
    backBtn.show();

  } else {
    backBtn.hide();
  }

  var nextBtn = this.nextBtn; //$(".activity .activity-navigation .btn-next");
  if (this.curSlide < this.totalSlides - 1) {
    nextBtn.show();

  } else {
    nextBtn.hide();
  }

  var doneBtn = this.doneBtn; //$(".activity .activity-navigation .btn-done");
  if (this.curSlide === this.totalSlides - 1) {
    doneBtn.show().removeClass("disabled");

  } else if (this.curSlide === this.totalSlides) {
    doneBtn.addClass("disabled");

  } else {
    doneBtn.hide();
  }
};

/**
 *
 */
ShellModel.prototype.showSlide = function() {
  var slides = $(".activity .activity-content-area .activity-slide");
  slides.hide();
  $(".activity .activity-navigation .activity-audio-caption .slide-audio").hide();
  this.pauseAllAudios();
  var ref = this;

  slides.each(function() {
    var slide = parseInt($(this).attr("data-slide"), 10);
    var after = $(this).attr("data-after");
    if (slide < ref.curSlide) {
      if (after === "show") {
        $(this).show();
      } else {
        $(this).hide();
      }
    } else if (slide === ref.curSlide) {
      $(this).show();
    } else {
      $(this).hide();
    }
  });

  // By default always disable the next button on slide load.
  if (this.curSlide > 1 && this.curSlide < this.totalSlides) {
    this.disableNextButton(true);
  }

  this.loadSlideElements();
  ref.playAudioHandler();
  this["step" + this.curSlide + "Handler"]();
};

/**
 *
 */
ShellModel.prototype.loadSlideElements = function() {
  this.slideElem = $('.activity-slide[data-slide="' + this.curSlide + '"]');
  this.checkBtn = this.slideElem.find('.btn-check');
  this.tryBtn = this.slideElem.find('.btn-try-again');
  this.showBtn = this.slideElem.find('.btn-show-me');
};

/**
 *
 */
ShellModel.prototype.pauseAllAudios = function() {
  $(".activity-audio").each(function() {
    try {
      $(this)[0].pause();
      $(this)[0].currentTime = 0;
    } catch (e) {
    }
  });
};

/**
 *
 * @param vol
 */
ShellModel.prototype.muteUnmuteAudio = function(vol) {
  $(".activity-audio").each(function() {
    $(this)[0].volume = vol;
  });
};

/**
 *
 */
ShellModel.prototype.playAudioHandler = function() {
  var ref = this;
  $(".activity .activity-navigation .activity-audio-caption .slide-audio[data-slide='" + this.curSlide + "']").show();

  if (this.audioPlayer && this.audioEventHandle) {
    this.audioPlayer.removeEventListener("ended", this.audioEventHandle);
  }

  // Get locale class.
  var locale;
  if (this.selectedLang === "en") {
    locale = '.english ';
  } else {
    locale = '.spanish ';
  }

  this.audioPlayer = null;
  var $audioPlayer = $(locale+".activity-audio[data-slide='" + this.curSlide + "']");
  if ($audioPlayer.length > 0) {

    ref.audioEventHandle = ref.onAudioEnded.bind(ref);

    this.audioPlayer = $audioPlayer[0];
    this.audioPlayer.play();
    this.audioPlayer.addEventListener("ended", ref.audioEventHandle);

    $(".activity .activity-navigation .btn-playpause ")
      .removeClass("btn-play")
      .addClass("btn-stop")
      .attr('aria-label', 'A pause button'); // TODO: use locale.
  }
};

/**
 *
 */
ShellModel.prototype.pauseAudioHandler = function() {
  if ($(".activity-audio[data-slide='" + this.curSlide + "']").length > 0) {
    $(".activity-audio[data-slide='" + this.curSlide + "']")[0].pause();
    $(".activity .activity-navigation .btn-playpause ")
      .removeClass("btn-stop")
      .addClass("btn-play")
      .attr('aria-label', 'A play button'); // TODO: use locale.
  }
};

/**
 *
 */
ShellModel.prototype.onAudioEnded = function() {
  $(".activity .activity-navigation .btn-playpause ")
    .removeClass("btn-stop")
    .addClass("btn-play")
    .attr('aria-label', 'A play button'); // TODO: use locale.

  if (this.onStepAudioEnded) {
    this.onStepAudioEnded();
  }
};

/**
 * Saves the assessment.
 */
ShellModel.prototype.saveAssessment = function() {

  if (this.params.get('type') !== 'submit_practice') {
    return;
  }

  var saveSettings = {
    success: function(responseIds) {
      console.log("save has been successful", responseIds);
    },
    error: function(e) {
      console.log("save has failed", e);
    },
    progress: function(e) {
      console.log("progress", e);
    }
  };
  itemsApp.save(saveSettings);
};

/**
 * Sets the initial state for a 'Save' button.
 * @param {object} saveBtn - the jQuery 'Save' button object.
 * @param {function=} callback - the click callback event handler.
 */
ShellModel.prototype.initSaveButton = function(saveBtn, callback) {
  this.disableButton(saveBtn, false);
  saveBtn.html("Save");
  saveBtn.show();
  saveBtn.off('click');
  saveBtn.on('click', function() {
    this.clickedSaveButton(saveBtn);
    if (callback !== null && callback !== undefined) {
      callback();
    }
  }.bind(this));
};

/**
 * Handles the 'Save' button processing.
 * @param {object} saveBtn - the jQuery 'Save' button object.
 */
ShellModel.prototype.clickedSaveButton = function(saveBtn) {
  saveBtn.html("Saved");
  this.disableButton(saveBtn, true);
  this.saveAssessment();
};

/**
 *
 * @param {string} learnosityId - the Learnosity item id.
 * @return {boolean} whether the question(s) are correct.
 */
ShellModel.prototype.checkAnswer = function(learnosityId, doValidation) {
  if (doValidation === undefined || doValidation === null) {
    doValidation = true;
  }

  var isCorrect = true;
  var item = itemsApp.getItems()[learnosityId];
  $.each(item.response_ids, function(idx, responseId) {
    var question = itemsApp.question(responseId);

    if (doValidation === true) {
      question.validate();
    }

    var valid = question.isValid();
    if (valid === undefined) {
      valid = false;
    }
    isCorrect = Boolean(isCorrect && valid);
    question.disable();

    var responses = question.getResponse();
    console.log(responses);
  });

  return isCorrect;
};

/**
 *
 * @param {string} learnosityId - the learnoisty reference id.
 * @param {function} callback - the callback.
 */
ShellModel.prototype.setQuestionChangedCallback = function(learnosityId, callback) {
  var item = itemsApp.getItems()[learnosityId];
  $.each(item.response_ids, function(idx, responseId) {
    var question = itemsApp.question(responseId);
    question.on("changed", callback);
  });
};

/**
 *
 * @param {string} learnosityId - the Learnosity item id.
 */
ShellModel.prototype.disableQuestion = function(learnosityId) {
  var item = itemsApp.getItems()[learnosityId];
  $.each(item.response_ids, function(idx, responseId) {
    itemsApp.question(responseId).disable();
  });
};

/**
 *
 * @param {string} learnosityId - the Learnosity item id.
 */
ShellModel.prototype.enableQuestion = function(learnosityId) {
  var item = itemsApp.getItems()[learnosityId];
  $.each(item.response_ids, function(idx, responseId) {
    itemsApp.question(responseId).enable();
  });
};

/**
 * Gets the responses to an Leanosity item.
 * @param {string} learnosityId - the Learnosity item id.
 * @return {Array} array of question responses
 */
ShellModel.prototype.getResponses = function(learnosityId) {
  var responses = [];
  var item = itemsApp.getItems()[learnosityId];
  $.each(item.response_ids, function(idx, responseId) {
    var question = itemsApp.question(responseId);
    responses.push(question.getResponse());
  });
  console.log(responses);
  return responses;
};

/**
 *
 * @return {RegExpMatchArray | null} returns whether this is a mobile/table device.
 */
ShellModel.prototype.isTablet = function() {
  var deviceAgent = navigator.userAgent.toLowerCase();
  return deviceAgent.match(/(mobi|tablet|iphone|ipod|ipad|android)/);
};

/**
 * Sets the disabled property on an element.
 * @param {object} elem - the jquery element
 * @param {boolean} value - the disabled value (true, false).
 */
ShellModel.prototype.disableButton = function(elem, value) {
  elem.prop("disabled", value);
  if (value === true) {
    elem.addClass("disabled");
  } else {
    elem.removeClass("disabled");
  }
};

/**
 * Disables the Next button.
 * @param {boolean} value - {true} to disable or {false} to enable
 */
ShellModel.prototype.disableNextButton = function(value) {
  this.disableButton(this.nextBtn, value);
};

/**
 * Disables the Done button.
 * @param {boolean} value - {true} to disable or {false} to enable
 */
ShellModel.prototype.disableDoneButton = function(value) {
  this.disableButton(this.doneBtn, value);
};

/**
 * Removes the Learnosity validation styles 'lrn_correct' and 'lrn_incorrect'.
 */
ShellModel.prototype.removeValidationStyles = function() {
  $('.lrn_incorrect').removeClass('lrn_incorrect');
  $('.lrn_correct').removeClass('lrn_correct');
  $('.big-correct').removeClass('big-correct');
  $('.big-incorrect').removeClass('big-incorrect');
  $('.small-correct').removeClass('small-correct');
  $('.small-incorrect').removeClass('small-incorrect');
  $('.mock-lrn-correct').removeClass('mock-lrn-correct');
  $('.mock-lrn-incorrect').removeClass('mock-lrn-incorrect');

  $('.correct').removeClass('correct');
  $('.incorrect').removeClass('incorrect');
};

/**
 *
 * @param enableCheck - whether to enable the check button or not.
 */
ShellModel.prototype.showCheckAnswerButtons = function(enableCheck) {
  this.checkBtn.show();
  this.tryBtn.hide();
  this.showBtn.hide();

  if (enableCheck === undefined || !enableCheck) {
    this.checkBtn.prop('disabled', true);
    this.checkBtn.addClass("disabled");
  } else {
    this.checkBtn.prop('disabled', false);
    this.checkBtn.removeClass("disabled");
  }
};

/**
 *
 */
ShellModel.prototype.showCheckAnswerCorrectButtons = function() {
  this.checkBtn.hide();
  this.tryBtn.hide();
  this.showBtn.hide();
};

/**
 *
 */
ShellModel.prototype.showCheckAnswerIncorrectButtons = function() {
  this.checkBtn.hide();
  this.tryBtn.show();
  this.showBtn.show();
};

/**
 *
 * @param enableCheck - whether to enable the check or disable the check.
 */
ShellModel.prototype.showTryAgainButtons = function(enableCheck) {
  this.showCheckAnswerButtons(enableCheck);
};

/**
 *
 */
ShellModel.prototype.showShowAnswerButtons = function() {
  this.checkBtn.hide();
  this.tryBtn.hide();
  this.showBtn.hide();
};

/**
 * Scrolls to the bottom of the given element.
 * @param {Element} $elem - the jQuery element to scroll
 */
ShellModel.prototype.scrollToBottom = function($elem) {
  var animateTime = 1000; // 1 second animation.
  $elem.animate({
    scrollTop: $elem.prop("scrollHeight")
  }, animateTime);
};

/**
 * Create a basic drag and drop widget.
 *
 * @param $dropArea
 * @param $dragBin
 * @param onChange
 * @param getDropArea
 */
ShellModel.prototype.createDragDrop = function($dropArea, $dragBin, onChange, getDropArea) {
  if (!getDropArea) {
    getDropArea = function() {
      return $dropArea;
    };
  }

  var ref = this;
  var $dragItems = $dragBin.find('.draggable-item');
  $dragItems
    .off('keydown')
    .on('keydown', function(event) {
      if (event.which === 13 || event.which === 32) {
        var $d = $(this).clone();
        $d.css({top: 0, left: 0});
        ref.addToDropArea($d, getDropArea($d), onChange);
      }
    });

  $dragItems
    .draggable()
    .draggable({
      revert: "invalid",
      stack: ".draggable-item",
      zIndex: 10001,
      helper: 'clone',
      containment: '.activity-content-area'
    })
    .draggable('enable')
    .show();

  $dropArea.droppable({
    accept: ".draggable-item",
    over: function(event, ui) {
      setTimeout(function() {
        ui.helper.addClass('placed');
      }, 1);
    },
    out: function(event, ui) {
      ui.helper.removeClass('placed');
    },
    drop: function(event, ui) {
      var $droppable = $(this);

      var $d = ui.helper;
      if (ui.draggable !== ui.helper) {
        $d = $d.clone();
      }

      var offset = $droppable.offset();
      var x = ui.offset.left - offset.left;
      var y = ui.offset.top - offset.top;

      ref.addToDropArea($d, $droppable, onChange, x, y);
    }
  });
  $dropArea.droppable("enable");
};

/**
 * Create a back and forth drag and drop widget.
 *
 * @param $dropArea1
 * @param $dropArea2
 * @param onChange
 * @param getDropArea
 */
ShellModel.prototype.createDropDrop = function($dropArea1, $dropArea2, onChange, getDropArea) {
  if (!getDropArea) {
    getDropArea = function($d) {
      if ($d.from[0] === $dropArea2[0])
        return $dropArea1;
      return $dropArea2;
    };
  }

  var ref = this;
  var $dropArea = $dropArea1.add($dropArea2);
  var $dragItems = $dropArea.find('.draggable-item');
  $dragItems.off('keydown');
  $dragItems.on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      var $d = $(this).clone();
      $d.css({
        top: 0,
        left: 0
      });
      $d.appendTo(getDropArea());
      onChange();
    }
  });
  $dragItems.draggable();
  $dragItems.draggable({
    revert: "invalid",
    stack: ".draggable-item",
    zIndex: 10001,
    containment: '.activity-content-area'
  });
  $dragItems.draggable('enable');
  $dragItems.show();

  $dropArea.droppable({
    accept: ".draggable-item",
    start: function(event, ui) {
      ui.helper.from = $(this);
    },
    drop: function(event, ui) {
      var $droppable = $(this);
      var $d = ui.helper;

      var offset = $droppable.offset();
      var x = ui.offset.left - offset.left;
      var y = ui.offset.top - offset.top;
      x = ref.clampDropXPosition($d, $droppable, x);
      y = ref.clampDropYPosition($d, $droppable, y);

      $d.removeClass('ui-draggable-dragging');
      $d.css('position', 'absolute');
      $d.css('left', x);
      $d.css('top', y);
      $d.appendTo($droppable);
      onChange();
    }
  });

  $dropArea1.droppable("enable");
};

/**
 *
 * @param $d
 * @param $dropArea
 * @param onChange
 * @param x
 * @param y
 */
ShellModel.prototype.addToDropArea = function($d, $dropArea, onChange, x, y) {

  // Clamp the position within the drop area.
  x = this.clampDropXPosition($d, $dropArea, x);
  y = this.clampDropYPosition($d, $dropArea, y);

  $d.removeClass('ui-draggable-dragging');
  $d.css('position', 'absolute');
  $d.css('left', x);
  $d.css('top', y);
  $d.appendTo($dropArea);

  onChange();

  // Make this object draggable.
  $d.draggable({
    revert: "false",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: "original",
    containment: '.activity-content-area',
    stop: function() {
      if (!$d.hasClass('placed')) {
        $d.draggable("destroy");
        $d.remove();

        onChange();
      }
    }
  });
  $d.off('keydown');
  $d.on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      $d.draggable("destroy");
      $d.remove();

      onChange();
    }
  });
};

/**
 * Clamp X position
 * @param {object} $d - the draggable
 * @param {object} $dropArea - the drop area
 * @param {number} x - the dropped x posistion
 * @return {number} the clamped x position
 */
ShellModel.prototype.clampDropXPosition = function($d, $dropArea, x) {
  x = parseInt(x, 10);
  var draggableWidth = $d.width();
  if (draggableWidth === 0) {
    draggableWidth = parseInt($d.find('img').attr('width'), 10);
  }
  var xMax = $dropArea.width() - draggableWidth;
  if (x < 0) {
    x = 0;
  } else if (x > xMax) {
    x = xMax;
  }
  return x;
};

/**
 * Clamp Y position
 * @param {object} $d - the draggable
 * @param {object} $dropArea - the drop area
 * @param {number} y - the dropped y posistion
 * @return {number} the clamped y position
 */
ShellModel.prototype.clampDropYPosition = function($d, $dropArea, y) {
  y = parseInt(y, 10);
  var draggableHeight = $d.height();
  if (draggableHeight === 0) {
    draggableHeight = parseInt($d.find('img').attr('height'), 10);
  }
  var yMax = $dropArea.height() - draggableHeight;
  if (y < 0) {
    y = 0;
  } else if (y > yMax) {
    y = yMax;
  }
  return y;
};

