/* global $ */

/* Start Jouve created for keyboard event*/
$(elem).find(".activity-question-option").off("keypress").on("keypress", function() {
  if ($(this).hasClass("disabled")) return;
  $(elem).find(".activity-question-option").removeClass("selected");
  $(this).addClass("selected");
  $(elem).find(".check-answer").text("Check Answer");
  $(elem).find(".correct-answer").hide();
  $(elem).find(".wrong-answer").hide();
});
/*End Jouve created for keyboard event*/

/* Start Jouve created for keyboard event*/
$(elem).find(".activity-question-option").off("keypress").on("keypress", function() {
  if ($(this).hasClass("disabled")) return;
  $(elem).find(".activity-question-option").removeClass("selected");
  $(this).addClass("selected");
  $(elem).find(".check-answer").text("Check Answer");
  $(elem).find(".correct-answer").hide();
  $(elem).find(".wrong-answer").hide();
});
/* End Jouve created for keyboard event*/
