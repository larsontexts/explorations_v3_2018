(function($) {

  // This a jQuery-UI Plugin Example
  $.widget('bil.tenframe', {
    model: {
      counterTypes: [],
      counters: []
    },

    options: {
    },
    _create: function() {
      var frame = ' <div style="margin-top:60px; position:relative; padding: 10px">\n' +
        '                                    <div class="understanding-overlay"\n' +
        '                                         style="position: absolute; width: 105%; height: 100%; left: 0px; top: 0px; z-index: -1;">\n' +
        '                                    </div>\n' +
        '                                    <img src="images/bilmk_exp_c04s01_001.png"\n' +
        '                                         alt="A bar divided into ten equal parts."\n' +
        '                                         class="slide-border d-block"\n' +
        '                                         width="413"\n' +
        '                                         height="46">\n' +
        '                                    <div class="drop-area2 drop-area">\n' +
        '                                        <div class="drop-area-block2 drop-area-block"\n' +
        '                                             style="top:10px; left:10px;"\n' +
        '                                             data-drop-left="2"\n' +
        '                                             data-drop-top="2">\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </div>';
      this.element.append(frame);
    },

    _destroy: function() {
    },

    _setOption: function(key, val) {
      this._superApply(arguments);
    },

    _addCounter: function() {
      //TODO: ...
    }
  });

// end of scope.
}(jQuery));