var gradeChapter = "19NA04_c09";
var chapterSection = "s05";
var baseExplorationPath = gradeChapter+"_"+chapterSection+"_exp1_int_exploration";
var explorationAudioPath = gradeChapter+chapterSection+"_exgr_";
var baseAudioPath = "../bil-assets/audio/";
var steps = [
	{
		'id':'screen1',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'01',
			'captionText':"Draw and shade a model to show one plus one plus two-thirds."
		}]
	},
	{
		'id':'screen2',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'02',
			'captionText':"Use your model to write the sum as a fraction."
		}]
	},
	{
		'id':'screen3',
		'audio': [{
			'file':'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'03',
			'captionText':"How can you write a fraction greater than 1 as a sum of a whole number and a fraction less than 1?  Explain."
		}]
	},
	{
		'id': 'screen4',
		'audio': [{
			'file': 'http://explorations.bigideasmath.com/fablevision/batch1_activities/'+baseExplorationPath+'/audio/'+explorationAudioPath+'04',
			'captionText': "Explain how you can write seven tenths as a sum of two fractions.  Draw a model to support your answer."
		}]
	}
];
