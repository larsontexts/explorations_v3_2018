/* global $ */
/* global MQ */
/* global ShellModel */
"use strict";

function onResize() {
  var windowHeight = $(window).height();
  var scrollHeight = windowHeight - (180);
  if (scrollHeight < 395) {
    scrollHeight = 395;
  }
  $(".scrollpane").height(scrollHeight);
}
// Map resize event to calculate new items
$(window).on("resize", onResize);

/**
 * Activity controller.
 * @param {number} slidesCount - the number of steps in the activity.
 * @constructor
 */
function ActivityModel(slidesCount) {
  var ref = this;
  ref.init(slidesCount);
  $(".activity-start-screen .start-btn").on("click", function() {
    $(".activity-start-screen").hide();
    $(".activity").show();
    ref.startActivity();
  });
}
ActivityModel.prototype = new ShellModel();
ActivityModel.prototype.constructor = ActivityModel;

/**
 * Step 1: Intro screen.
 */
ActivityModel.prototype.step1Handler = function() {

  // Initialize the window size.
  onResize();

  // Render static MathQuill elements
  $(".quill").each(function(index) {
    var elem = $(this)[0];
    MQ.StaticMath(elem);
  });

  this.disableNextButton(false); // Enable 'Next' button.
};

/**
 * Step 2: Learnosity shading
 */
ActivityModel.prototype.step2Handler = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  $(elem+".question").show();
  $(elem+".answer").hide();

  this.removeValidationStyles(); // Remove validation styles.
  this.showCheckAnswerButtons(true); // Update button visibility.
  this.disableNextButton(true); // Disable 'Next' button.
  this.enableQuestion('19NA05_c08_s06_exp1_int_exploration_01'); // Enable the inputs.

  // Add click handlers.
  this.checkBtn.off('click').on('click', this.step2CheckAnswer.bind(this));
  this.tryBtn.off('click').on('click', this.step2TryAgain.bind(this));
  this.showBtn.off('click').on('click', this.step2ShowAnswer.bind(this));
};

/**
 * Step 2: Check Answer handler.
 */
ActivityModel.prototype.step2CheckAnswer = function() {
  var elem = ".activity-slide[data-slide='2'] ";

  // Use ItemApi + Question Method to run validation on the question.
  var isCorrect = this.checkAnswer('19NA05_c08_s06_exp1_int_exploration_01');

  // Remove validation styles from the blocks.
  this.removeValidationStyles();

  if (isCorrect) {
    this.showCheckAnswerCorrectButtons(); // Update button visibility.
    this.disableNextButton(false); // Enable 'Next' button.
    $(elem+".question").addClass('big-correct'); // Add the correct style.

  } else {
    this.showCheckAnswerIncorrectButtons(); // Update button visibility.
    $(elem+".question").addClass('big-incorrect').show();
  }
};

/**
 * Step 2: Try Again handler.
 */
ActivityModel.prototype.step2TryAgain = function() {
  this.enableQuestion('19NA05_c08_s06_exp1_int_exploration_01'); // Enable input.
  this.removeValidationStyles(); // Remove all (in)correct styles
  this.showTryAgainButtons(true); // Update button visibility.
};

/**
 * Step 2: Show Answer handler.
 */
ActivityModel.prototype.step2ShowAnswer = function() {
  this.showShowAnswerButtons(); // Update button visibility.
  this.disableNextButton(false); // Enable 'Next' button.
  this.disableQuestion('19NA05_c08_s06_exp1_int_exploration_01a'); // Disable input.

  var elem = ".activity-slide[data-slide='2'] ";
  $(elem+".question").hide(); // Hide the question.
  $(elem+".answer").show(); // Show the answer.
  $(elem+".answer").addClass('big-correct'); // Add the correct style.
};

/**
 * Step 3: solve the Learnosity fill-in
 */
ActivityModel.prototype.step3Handler = function() {
  var elem = ".activity-slide[data-slide='3'] ";

  // Update the main question to match the new content
  $('.main_question').addClass('hideElement');

  // Init variables.
  var checkBtn = $(elem + '#check_btn');
  var tryBtn = $(elem + '#try_btn');
  var showBtn = $(elem + '#show_btn');

  // hide learnosity check button
  var learnCheck = $('.lrn .lrn_btn.lrn_validate');
  learnCheck.hide();

  $(elem + '.question1').show();
  $(elem + '.answer1').hide();

  // Show buttons.
  checkBtn.show();
  tryBtn.hide();
  showBtn.hide();

  this.scrollToBottom($('.scrollpane'));
  this.removeValidationStyles(); // Remove all (in)correct styles
  this.enableQuestion('19NA05_c08_s06_exp1_int_exploration_02'); // Enable the inputs.
  this.disableNextButton(true); // disable next button.

  // Add click handlers.
  checkBtn.off('click').on('click', this.step3CheckAnswer.bind(this));
  tryBtn.off('click').on('click', this.step3TryAgain.bind(this));
  showBtn.off('click').on('click', this.step3ShowAnswer.bind(this));
};

/**
 * Step 3: Check Answer handler.
 */
ActivityModel.prototype.step3CheckAnswer = function() {
  // Use ItemApi + Question Method to run validation on the question.
  var isCorrect = this.checkAnswer('19NA05_c08_s06_exp1_int_exploration_02');
  if (isCorrect) {
    this.showCheckAnswerCorrectButtons();
    this.disableNextButton(false); // Enable 'Next' button.

    var elem = ".activity-slide[data-slide='3'] ";
    $(elem + '.question1').hide(); // Hide the question
    $(elem + '.answer1').show(); // Show the answer.
    $(elem + '.answer1').addClass('small-correct');

  } else {
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 * Step 3: Try Again handler.
 */
ActivityModel.prototype.step3TryAgain = function() {
  this.enableQuestion('19NA05_c08_s06_exp1_int_exploration_02');
  this.showTryAgainButtons(true); // Update button visibility
  this.removeValidationStyles(); // Remove all (in)correct styles
};

/**
 * Step 3: Show Answer handler.
 */
ActivityModel.prototype.step3ShowAnswer = function() {
  this.disableNextButton(false); // Enable 'Next' button.
  this.showShowAnswerButtons(); // Hide the buttons

  var elem = ".activity-slide[data-slide='3'] ";
  $(elem + '.question1').hide(); // Hide the question.
  $(elem + '.answer1').show(); // Show the answer.
  $(elem + '.answer1').addClass('small-correct');
};

/**
 * Step 4: Learnosity essay
 */
ActivityModel.prototype.step4Handler = function() {
  var elem = ".activity-slide[data-slide='4'] ";

  this.scrollToBottom($('.scrollpane'));
  this.removeValidationStyles(); // Remove all (in)correct styles
  this.disableDoneButton(true); // Enable the done button.
  this.enableQuestion('19NA05_c08_s06_exp1_int_exploration_03');

  // Add the click save handler.
  var saveBtn = $('.btn-save');
  this.initSaveButton(saveBtn, this.saveButtonNext4.bind(this));
};

/**
 * Step 5: Learnosity essay
 */
ActivityModel.prototype.step5Handler = function() {
  var elem = ".activity-slide[data-slide='4'] ";

  this.scrollToBottom($('.scrollpane'));
  this.removeValidationStyles(); // Remove all (in)correct styles
  this.disableDoneButton(true); // Enable the done button.
  this.enableQuestion('19NA05_c08_s06_exp1_int_exploration_04');

  // Add the click save handler.
  var saveBtn = $('.btn-save');
  this.initSaveButton(saveBtn, this.saveButtonDone.bind(this));
};

/**
 * Wait until the save button is pressed then enable the Next button.
 */
ActivityModel.prototype.saveButtonNext4 = function() {
  this.disableNextButton(false);
  this.disableQuestion('19NA05_c08_s06_exp1_int_exploration_03');
};

/**
 * Wait until the save button is pressed then enable the Done button.
 */
ActivityModel.prototype.saveButtonDone = function() {
  this.disableDoneButton(false);
  this.disableQuestion('19NA05_c08_s06_exp1_int_exploration_04');
};

/**
 * Step 6: End screen.
 */
ActivityModel.prototype.step6Handler = function() {
  $(".btn-save").hide();
};

/**
 * Reset handler.
 */
ActivityModel.prototype.resetActivityHandler = function() {
  this.disableNextButton(false); // Enable the next button
  this.disableDoneButton(true); // Disable the done button
  this.removeValidationStyles(); // Remove all (in)correct styles

  // Reset main question
  $('.main_question').removeClass('hideElement');
};
