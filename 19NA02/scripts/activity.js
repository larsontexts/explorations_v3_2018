/* global $ */
/* itemsApp */
/* global ShellModel */
"use strict";

/**
 * Activity controller.
 * @constructor
 */
function ActivityModel() {
  var ref = this;
  ref.init(6);
  $(".activity-start-screen .start-btn").on("click", function() {
    $(".activity-start-screen").hide();
    $(".activity").show();
    ref.startActivity();
  });
}
ActivityModel.prototype = new ShellModel();
ActivityModel.prototype.constructor = ActivityModel;

/**
 * Step 1: Initial screen.
 */
ActivityModel.prototype.step1Handler = function() {
  this.removeValidationStyles(); // Remove validation styles.
  this.disableNextButton(false); // Enable the next button.
};

/**
 * Step 2: Drag counters to model 32.
 */
ActivityModel.prototype.step2Handler = function() {
  var ref = this;
  var elem = ".activity-slide[data-slide='2'] ";
  var elemD = ".activity-slide[data-slide='1'] ";
  $(elemD+'.drop-area1 .drop-area-block1').droppable("disable");

  // Init variables
  this.checkBtn = $(elem+'#check_btn');
  this.tryBtn = $(elem+'#try_btn');
  this.showBtn = $(elem+'#show_btn');

  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  var dragItems = $(elem+'.drag-bin .draggable-item');

  dragItems.draggable();
  dragItems.draggable({
    revert: "invalid",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: 'clone',
    containment: '.activity-content-area'
  });
  dragItems.draggable('enable');
  dragItems.show();

  dropArea.droppable({
    accept: ".draggable-item",
    over: function(event, ui) {
      ui.helper.addClass('placed');
    },
    out: function(event, ui) {
      ui.helper.removeClass('placed');
    },
    drop: function(event, ui) {
      var droppable = $(this);
      var draggable = ui.helper;
      var d = draggable;
      if (ui.draggable !== ui.helper) {
        d = draggable.clone();
      }

      var offset = droppable.offset();
      var x = ui.offset.left - offset.left;
      var y = ui.offset.top - offset.top;
      ref.step2AddDraggable(d, droppable, x, y);
    }
  });
  dropArea.droppable("enable");

  this.showCheckAnswerButtons(); // Set button states.
  this.disableNextButton(true); // Disable the next button.
  this.updateCheck1();

  // Checking Answer Handle.
  this.checkBtn
    .off("click")
    .on("click", this.step2CheckAnswer.bind(this));

  // Try Again Handle.
  this.tryBtn
    .off("click")
    .on("click", this.step2TryAgain.bind(this));

  // Show Answer Handle.
  this.showBtn
    .off("click")
    .on("click", this.step2ShowAnswer.bind(this));

  dragItems.off('keydown').on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      ref.step2AddDraggable($(this).clone(), dropArea);
    }
  });
};

/**
 *
 * @param d
 * @param droppable
 * @param x
 * @param y
 */
ActivityModel.prototype.step2AddDraggable = function(d, droppable, x, y) {
  var ref = this;

  // Clamp X
  var draggableWidth = parseInt(d.find('img').attr('width'), 10);
  var xMax = droppable.width() - draggableWidth;
  if (x < 0) {
    x = 0;
  } else if (x > xMax) {
    x = xMax;
  }

  // Clamp Y
  var draggableHeight = parseInt(d.find('img').attr('height'), 10);
  var yMax = droppable.height() - draggableHeight;
  if (y < 0) {
    y = 0;
  } else if (y > yMax) {
    y = yMax;
  }

  d.removeClass('ui-draggable-dragging');
  d.css('position', 'absolute');
  d.css('left', x);
  d.css('top', y);
  d.appendTo(droppable);

  this.updateCheck1();

  // Make this object draggable.
  d.draggable({
    revert: "false",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: "original",
    containment: '.activity-content-area',
    stop: function() {
      if (!d.hasClass('placed')) {
        d.draggable("destroy");
        d.remove();
        ref.updateCheck1();
      }
    }
  });

  d.off('keydown').on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      event.stopPropagation();
      d.draggable("destroy");
      d.remove();
      ref.updateCheck1();
    }
  });
};

/**
 * Check the answer
 * @return {boolean} whether the answer is correct or not.
 * @private
 */
ActivityModel.prototype.step2IsCorrect = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var tens = $(elem+'.drop-area1 .drop-area-block1 .tens-unit').length;
  var ones = $(elem+'.drop-area1 .drop-area-block1 .ones-unit').length;
  return (tens*10 + ones === 32);
};

/**
 *
 */
ActivityModel.prototype.step2CheckAnswer = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var understandingOverlay = $(elem+'.understanding-overlay');
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  var dragItems = $(elem+'.drag-bin .draggable-item');

  // Disable dragging.
  dragItems.hide();
  dragItems.draggable('disable');

  // Disable the draggables.
  dropArea.find('.draggable-item').draggable('disable');

  // Check answer.
  var isCorrect = this.step2IsCorrect();
  if (isCorrect) {
    understandingOverlay.addClass('lrn_correct');
    this.showCheckAnswerCorrectButtons();
    this.disableNextButton(false); // Enable the next button.

  } else {
    understandingOverlay.addClass('lrn_incorrect');
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 *
 */
ActivityModel.prototype.step2TryAgain = function() {
  this.showTryAgainButtons(); // Update button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  // Re-enable the dragging.
  var dragItems = $('.draggable-item');
  dragItems.show();
  dragItems.draggable().draggable('enable');
};

/**
 *
 */
ActivityModel.prototype.step2ShowAnswer = function() {
  this.showShowAnswerButtons(); // Update the button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles.
  this.disableNextButton(false); // Enable the next button.

  var elem = ".activity-slide[data-slide='2'] ";
  var dragItems = $(elem+'.drag-bin .draggable-item');

  // Show the correct style.
  var understandingOverlay = $(elem+'.understanding-overlay');
  understandingOverlay.addClass('lrn_correct');

  // Remove all items.
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  dropArea.empty();

  // Add the proper amount to the drop area.
  var i;
  var item;
  var clone;
  var unitWidth = 40;
  var unitHeight = 40;

  var alignLeft = 0;
  var alignTop = 0;
  item = dragItems.eq(0).clone();
  item.show();
  for (i=0; i<3; i+=1) {
    clone = item.clone();
    clone.appendTo(dropArea);
    clone.css('left', alignLeft + i*unitWidth);
  }

  item = dragItems.eq(1).clone();
  item.show();
  for (i=0; i<2; i+=1) {
    clone = item.clone();
    clone.appendTo(dropArea);
    clone.css('left', alignLeft + 3*unitWidth);
    clone.css('top', alignTop + i*unitHeight);
  }

  // Disable the draggables.
  dropArea.find('.draggable-item').draggable().draggable('disable');
};

/**
 * Updates the Slide 2 check button enabled/disabled state.
 */
ActivityModel.prototype.updateCheck1 = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var checkBtn = $(elem+'#check_btn');
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  if (dropArea[0].childElementCount > 0) {
    checkBtn
      .prop('disabled', false)
      .removeClass('disabled');
  } else {
    checkBtn
      .prop('disabled', true)
      .addClass('disabled');
  }
};

/**
 * Step 3: Drag counters to model 27.
 */
ActivityModel.prototype.step3Handler = function() {
  var ref = this;

  var elem = ".activity-slide[data-slide='2'] ";

  // Remove all (in)correct styles
  this.removeValidationStyles();

  var dragItems = $(elem+'.drag-bin .draggable-item');
  dragItems.draggable();
  dragItems.draggable({
    revert: "invalid",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: 'clone',
    containment: '.activity-content-area'
  });
  dragItems.draggable('enable');
  dragItems.show();

  // Init variables
  this.checkBtn = $(elem+'#check_btn');
  this.tryBtn = $(elem+'#try_btn');
  this.showBtn = $(elem+'#show_btn');
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');

  dropArea.droppable({
    accept: ".draggable-item",
    over: function(event, ui) {
      ui.helper.addClass('placed');
    },
    out: function(event, ui) {
      ui.helper.removeClass('placed');
    },
    drop: function(event, ui) {
      var droppable = $(this);
      var draggable = ui.helper;
      var d = draggable;
      if (ui.draggable !== ui.helper) {
        d = draggable.clone();
      }

      var offset = droppable.offset();
      var x = ui.offset.left - offset.left;
      var y = ui.offset.top - offset.top;
      ref.step3AddDroppable(d, droppable, x, y);
    }
  });

  this.showCheckAnswerButtons(); // Set button default states.
  this.disableNextButton(true); // Disable the next button.
  this.updateCheck2();

  // Handle Checking Answer.
  this.checkBtn
    .off("click")
    .on("click", this.step3CheckAnswer.bind(this));

  // Handle Try Again.
  this.tryBtn
    .off("click")
    .on("click", this.step3TryAgain.bind(this));

  // Handle Show Answer.
  this.showBtn
    .off("click")
    .on("click", this.step3ShowAnswer.bind(this));
};

/**
 *
 * @param d
 * @param droppable
 * @param x
 * @param y
 */
ActivityModel.prototype.step3AddDroppable = function(d, droppable, x, y) {
  var ref = this;

  // Clamp X
  var draggableWidth = parseInt(d.find('img').attr('width'), 10);
  var xMax = droppable.width() - draggableWidth;
  if (x < 0) {
    x = 0;
  } else if (x > xMax) {
    x = xMax;
  }

  // Clamp Y
  var draggableHeight = parseInt(d.find('img').attr('height'), 10);
  var yMax = droppable.height() - draggableHeight;
  if (y < 0) {
    y = 0;
  } else if (y > yMax) {
    y = yMax;
  }

  d.removeClass('ui-draggable-dragging');
  d.css('position', 'absolute');
  d.css('left', x);
  d.css('top', y);
  d.appendTo(droppable);

  ref.updateCheck2();

  // Make this object draggable.
  d.draggable({
    revert: "false",
    stack: ".draggable-item",
    zIndex: 10001,
    helper: "original",
    containment: '.activity-content-area',
    stop: function() {
      if (!d.hasClass('placed')) {
        d.draggable("destroy");
        d.remove();
        ref.updateCheck2();
      }
    }
  });

  d.off('keydown').on('keydown', function(event) {
    if (event.which === 13 || event.which === 32) {
      event.stopPropagation();
      d.draggable("destroy");
      d.remove();
      ref.updateCheck1();
    }
  });
};

/**
 * Check the answer
 * @return {boolean} whether the answer is correct or not.
 * @private
 */
ActivityModel.prototype.step3IsCorrect = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var tens = $(elem+'.drop-area1 .drop-area-block1 .tens-unit').length;
  var ones = $(elem+'.drop-area1 .drop-area-block1 .ones-unit').length;
  return (tens*10 + ones === 59);
};

/**
 *
 */
ActivityModel.prototype.step3CheckAnswer = function() {

  var elem = ".activity-slide[data-slide='2'] ";
  var understandingOverlay = $(elem+'.understanding-overlay');

  // Hide the draggables.
  var dragItems = $(elem+'.drag-bin .draggable-item');
  dragItems.hide();

  // Disable the draggables.
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  var items = dropArea.find('.draggable-item');
  items.draggable().draggable('disable');

  // Check answer.
  var isCorrect = this.step3IsCorrect();
  if (isCorrect) {
    understandingOverlay.addClass('lrn_correct');
    this.showCheckAnswerCorrectButtons(); // Update button visibility.
    this.disableNextButton(false); // Enable the next button.

  } else {
    understandingOverlay.addClass('lrn_incorrect');
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 *
 */
ActivityModel.prototype.step3TryAgain = function() {
  this.showTryAgainButtons(); // Update button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles.

  var elem = ".activity-slide[data-slide='2'] ";

  // Re-enable the dragging.
  var dragItems = $(elem+'.draggable-item');
  dragItems
    .draggable()
    .draggable('enable')
    .show();
};

/**
 *
 */
ActivityModel.prototype.step3ShowAnswer = function() {
  this.removeValidationStyles(); // Remove drop area styles.
  this.showShowAnswerButtons(); // Update the button visibility.
  this.disableNextButton(false); // Enable the next button.

  var elem = ".activity-slide[data-slide='2'] ";

  // Show the correct style.
  var understandingOverlay = $(elem+'.understanding-overlay');
  understandingOverlay.addClass('lrn_correct');

  // Remove all items.
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  dropArea.empty();

  // Add the proper amount to the drop area.
  var i;
  var item;
  var clone;
  var left = 0;
  var top = 0;
  var unitWidth = 40;
  var unitHeight = 38;

  // Model 32
  var dragItems = $(elem+'.drag-bin .draggable-item');
  item = dragItems.eq(0).clone();
  item.show();
  for (i=0; i<3; i+=1) {
    left= i*unitWidth;
    top = 0;

    clone = item.clone();
    clone.css('left', left);
    clone.appendTo(dropArea);
  }
  item = dragItems.eq(1).clone();
  item.show();
  for (i=0; i<2; i+=1) {
    left = 3*unitWidth;
    top = i*unitHeight;

    clone = item.clone();
    clone.css('left', left);
    clone.css('top', top);
    clone.appendTo(dropArea);
  }

  // Model 27
  var alignLeft = 200;
  var alignTop = 0;
  item = dragItems.eq(0).clone();
  item.show();
  for (i=0; i<2; i+=1) {
    left = alignLeft + i*unitWidth;
    top = alignTop;

    clone = item.clone();
    clone.css('left', left);
    clone.css('top', top);
    clone.appendTo(dropArea);
  }

  item = dragItems.eq(1).clone();
  item.show();
  for (i=0; i<7; i+=1) {
    left = alignLeft + 2*unitWidth + Math.floor(i/5)*unitWidth;
    top = alignTop + Math.floor(i%5)*unitHeight;

    clone = item.clone();
    clone.css('left', left);
    clone.css('top', top);
    clone.appendTo(dropArea);
  }
};

/**
 * Updates the Slide 3 check button enabled/disabled state.
 */
ActivityModel.prototype.updateCheck2 = function() {
  var elem = ".activity-slide[data-slide='2'] ";
  var checkBtn = $(elem+'#check_btn');
  var dropArea = $(elem+'.drop-area1 .drop-area-block1');
  if (dropArea[0].childElementCount > 5) {
    checkBtn
      .prop('disabled', false)
      .removeClass('disabled');
  } else {
    checkBtn
      .prop('disabled', true)
      .addClass('disabled');
  }
};

/**
 * Step 4: enter the sum 59.
 */
ActivityModel.prototype.step4Handler = function() {
  var ref = this;

  this.removeValidationStyles(); // Remove answer styles.

  // Replace Learnosity 'Check Answer' text.
  $(".lrn .lrn_btn.lrn_validate span").text("Check");

  var elemD = ".activity-slide[data-slide='2'] ";

  // Disable the dragging off the area.
  $(elemD+'.draggable-item')
    .draggable()
    .draggable('enable')
    .draggable({
      containment: '.activity-slide[data-slide=\'2\'] .drop-area1 .drop-area-block1',
      stop: function() {}
    });
  $(elemD+'.drop-area-block1')
    .droppable({
      drop: function() {}
    });

  var elem = ".activity-slide[data-slide='4'] ";

  // Init variables
  var learnBtn = $(elem+".lrn_validate");
  var checkBtn = $(elem+'#check_btn');
  var tryBtn = $(elem+'#try_btn');
  var showBtn = $(elem+'#show_btn');

  $('.question').show();
  $('.answer').hide();

  // Hide buttons
  learnBtn.hide();

  this.showCheckAnswerButtons(true);
  this.disableNextButton(true); // Disable the next button.
  this.enableQuestion('19NA02_c04s01_exp1_int_exploration_1'); // Enable the learnosity question.

  // Checking Answer Handle.
  checkBtn
    .off("click")
    .on("click", this.step4CheckAnswer.bind(this));

  // Handle Try Again.
  tryBtn
    .off("click")
    .on("click", this.step4TryAgain.bind(this));

  // Handle Show Answer.
  showBtn
    .off("click")
    .on("click", this.step4ShowAnswer.bind(this));
};

/**
 *
 */
ActivityModel.prototype.step4CheckAnswer = function() {
  // Validate the question and answer.
  var isCorrect = this.checkAnswer('19NA02_c04s01_exp1_int_exploration_1');
  if (isCorrect) {
    this.showCheckAnswerCorrectButtons(); // Update button visibility.
    this.disableNextButton(false); // Enable the next button.

  } else {
    // understandingOverlay.addClass('lrn_incorrect');
    this.showCheckAnswerIncorrectButtons();
  }
};

/**
 *
 */
ActivityModel.prototype.step4TryAgain = function() {
  this.enableQuestion('19NA02_c04s01_exp1_int_exploration_1');
  this.showTryAgainButtons(true); // Update button visibility.
  this.removeValidationStyles(); // Remove all (in)correct styles.
};

/**
 *
 */
ActivityModel.prototype.step4ShowAnswer = function() {
  this.removeValidationStyles(); // Remove all (in)correct styles.
  this.showShowAnswerButtons(); // Update the button visibility.
  this.disableNextButton(false); // Enable the next button.

  $('.question').hide();
  $('.answer .understanding-overlay').addClass('correct');
  $('.answer').show();
};

/**
 * Step 5: Listen to the audio.
 */
ActivityModel.prototype.step5Handler = function() {
  this.removeValidationStyles(); // Remove all (in)correct styles.
  this.disableDoneButton(true); // Disable the done button.

  // Show the answer, hide the question.
  $('.answer').show();
  $('.question').hide();

  // Add a handler to listen to the audio player.
  this.audioPlayer.addEventListener("ended", this.waitForAudio.bind(this));
};

/**
 * Step 6: End screen.
 */
ActivityModel.prototype.step6Handler = function() {
 // Nothing. Just listen to the audio.
};

/**
 * Reset
 */
ActivityModel.prototype.resetActivityHandler = function() {
  var ref = this;
  for (var i = 1; i <= this.curSlide; i++) {
    var elem = ".activity-slide[data-slide='"+i+"'] ";
    if ($(elem+" .draggable-item").hasClass("ui-draggable")) {
      $(elem+" .draggable-item").draggable("destroy");
    }
    if ($(elem+" .drop-area"+i+" .drop-area-block"+i).hasClass("ui-droppable")) {
      $(elem+" .drop-area"+i+" .drop-area-block"+i).droppable("destroy");
    }
  }
  $(".activity .drop-area .drop-area-block .draggable-item").remove();

  // Remove all counters from the drop area
  $(".activity-slide[data-slide='2'] .active .drop-area1 .drop-area-block1").empty();

  // Clean-up events.
  $(".activity-audio[data-slide='5']").each(function(ind, elem) {
    $(this)[0].removeEventListener("ended", ref.waitForAudio.bind(ref));
  });

  this.disableNextButton(false); // Enable the next button
  this.disableDoneButton(true); // Disable the done button
  this.removeValidationStyles(); // Remove all (in)correct styles
};

/**
 * Wait until the audio has finished then enable the audio.
 */
ActivityModel.prototype.waitForAudio = function() {
  this.disableDoneButton(false);
};
