function appendSampleSplashText(){
	let $target = $(".activity-start-screen");

	let topRow = document.createElement('div');
	topRow.classList.add('preview-header');
	topRow.innerText = "Exploration Coming Soon!";
	$target.append(topRow);

	let bottomRow = document.createElement('div');
	bottomRow.classList.add('preview-header', 'preview-header-bottom-row');
	bottomRow.innerText = "Click to see a sample";
	$target.append(bottomRow);
}